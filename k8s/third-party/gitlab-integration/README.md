# Gitlab Integration

## Adding your cluster to Gitlab
Gitlab has written a guide for adding a new or existing cluster for integration in Gitlab.

Use the following link: https://docs.gitlab.com/ee/user/project/clusters/add_remove_clusters.html