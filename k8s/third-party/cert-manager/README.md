# Cert-Manager resources

## Installing Cert-Manager

Cert-Manager is required to use this chart. To install Cert-Manager you can use the link below.

link: https://cert-manager.io/docs/installation/kubernetes/

### Configuration

Helm values to define the cluster issuer: 

| Parameter  | Description      | Default |
| ---------- | ---------------  | ------- |
| `issuer.acme.server` | The acme server/endpoint cert-manager requests certificates from. You can use letsencrypt for production through `https://acme-v02.api.letsencrypt.org/directory`. The default value is for staging environments.  |   `https://acme-staging-v02.api.letsencrypt.org/directory`.  |
| `issuer.acme.email` | Domain owner's email | "" |

Example command for providing a clusterissuer for production:
```
helm install cert-manager-clusterissuer-prod cert-manager-clusterissuer/ --set issuer.acme.email="foo@bar.com",issuer.acme.server="https://acme-v02.api.letsencrypt.org/directory"
```