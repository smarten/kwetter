# Installing an Ingress Controller

Installing an ingress-controller can be done through the following guide: https://kubernetes.github.io/ingress-nginx/deploy/

### CAUTION!!!

As of 2020-03-30, DigitalOcean has a bug with nginx-ingress. This makes it so SSL is impossible. See the yaml file included in this directory and follow the link in the comment to learn more.