﻿using System;
using System.Threading;
using System.Threading.Tasks;
using FluentAssertions;
using Grpc.Core;
using Grpc.Core.Testing;
using GrpcUser;
using MediatR;
using Microsoft.Extensions.Logging.Abstractions;
using Moq;
using User.Application.EventHandling.RegisterUser;
using User.Web.Services;
using Xunit;

namespace User.Web.UnitTests.Services
{
    public class UserGrpcServiceTest
    {
        [Fact]
        public async Task Should_return_valid_response_when_valid_user_registration_received()
        {
            // Arrange
            var registerUserRequest = new RegisterUserRequest
            {
                User = new GrpcUser.User
                {
                    Email = "johndoe@gmail.com",
                    Id = "123",
                    Username = "johndoe"
                }
            };

            var mediatorMock = new Mock<IMediator>();
            var sut = new UserGrpcService(mediatorMock.Object, new NullLogger<UserGrpcService>());

            var serverCallbackContext = TestServerCallContext.Create(
                It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<DateTime>(),
                It.IsAny<Metadata>(),
                It.IsAny<CancellationToken>(),
                It.IsAny<string>(),
                It.IsAny<AuthContext>(),
                It.IsAny<ContextPropagationToken>(),
                It.IsAny<Func<Metadata, Task>>(),
                It.IsAny<Func<WriteOptions>>(),
                It.IsAny<Action<WriteOptions>>()
            );

            // Act
            var response = await sut.RegisterUser(registerUserRequest, serverCallbackContext);

            // Assert
            response.Should().NotBeNull();
            serverCallbackContext.Status.StatusCode.Should().Be(StatusCode.OK);
            serverCallbackContext.Status.Detail.Should().Be("User successfully registered.");
        }

        [Fact]
        public async Task Should_send_user_registration_command_when_user_registration_is_received()
        {
            // Arrange
            var registerUserRequest = new RegisterUserRequest
            {
                User = new GrpcUser.User
                {
                    Email = "johndoe@gmail.com",
                    Id = "123",
                    Username = "johndoe"
                }
            };

            var mediatorMock = new Mock<IMediator>();
            var sut = new UserGrpcService(mediatorMock.Object, new NullLogger<UserGrpcService>());

            var serverCallbackContext = TestServerCallContext.Create(
                It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<DateTime>(),
                It.IsAny<Metadata>(),
                It.IsAny<CancellationToken>(),
                It.IsAny<string>(),
                It.IsAny<AuthContext>(),
                It.IsAny<ContextPropagationToken>(),
                It.IsAny<Func<Metadata, Task>>(),
                It.IsAny<Func<WriteOptions>>(),
                It.IsAny<Action<WriteOptions>>()
            );

            // Act
            await sut.RegisterUser(registerUserRequest, serverCallbackContext);

            // Assert
            mediatorMock.Verify(mock =>
                mock.Send(
                    It.Is<RegisterUserCommand>(command =>
                        command.Email == "johndoe@gmail.com" &&
                        command.AuthUserId == "123" &&
                        command.Username == "johndoe"),
                    It.IsAny<CancellationToken>()
                    )
                );
        }
    }
}
