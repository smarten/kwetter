﻿using System;
using EventStore;
using EventStore.ClientAPI;
using HealthChecks.UI.Client;
using MassTransit;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.Extensions.Hosting;
using User.Application.Auth;
using User.Application.Auth.AuthZero;
using User.Application.Auth.AuthZero.Config;
using User.Application.EventHandling.RegisterUser;
using User.Application.EventHandling.ReplayUserRegistation;
using User.Domain.Core;
using User.Infrastructure.Repositories;
using User.Web.Services;

namespace User.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddCustomOptions(Configuration)
                .AddApplicationServices()
                .AddHealthChecks(Configuration)
                .AddEventStore(Configuration)
                .AddEventBus(Configuration);

            services.AddGrpc();

            services.AddMediatR(typeof(RegisterUserCommand));
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            // Replay user registration for the already existing users in Auth0
            using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                var mediator = serviceScope.ServiceProvider.GetRequiredService<IMediator>();
                mediator.Send(new ReplayUserRegistrationCommand()).Wait();
            }

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHealthChecks("/hc", new HealthCheckOptions
                {
                    Predicate = _ => true,
                    ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
                });

                endpoints.MapHealthChecks("/liveness", new HealthCheckOptions
                {
                    Predicate = r => r.Name.Contains("self")
                });

                endpoints.MapGrpcService<UserGrpcService>();
            });
        }
    }

    public static class ServiceCollectionExtensionMethods
    {
        public static IServiceCollection AddCustomOptions(this IServiceCollection services,
            IConfiguration configuration)
        {
            services.AddOptions();

            services.Configure<AuthZeroConfig>(configuration.GetSection("auth0"));

            return services;
        }

        // TODO: Add EventStore health check. AspNetCore.HealthChecks.EventStore says ConnectionString is invalid.
        public static IServiceCollection AddHealthChecks(this IServiceCollection services,
            IConfiguration configuration)
        {
            // RabbitMQ health check is configured by MassTransit at services.AddEventBus(...)
            services
                .AddHealthChecks()
                .AddCheck("self", () => HealthCheckResult.Healthy());

            return services;
        }

        public static IServiceCollection AddEventStore(this IServiceCollection services,
            IConfiguration configuration)
        {
            var eventStoreConnectionString = configuration.GetValue<string>("eventStoreConnectionString");
            services.AddSingleton<IEventStoreConnection>(options =>
            {
                var settings = ConnectionSettings.Create();
                settings.KeepRetrying();
                settings.KeepReconnecting();

                var connection = EventStoreConnection.Create(settings, new Uri(eventStoreConnectionString));
                connection.ConnectAsync().Wait();

                return connection;
            });

            services.AddSingleton<IEventStore, EventStoreEventStore>(provider =>
            {
                var eventStoreConnection = provider.GetRequiredService<IEventStoreConnection>();
                return new EventStoreEventStore(eventStoreConnection, typeof(IEventSourcingRepository<>).Assembly);
            });

            return services;
        }

        public static IServiceCollection AddApplicationServices(this IServiceCollection services)
        {
            services.AddHttpClient<IAuthClient, AuthZeroClient>();

            services.AddScoped(typeof(IEventSourcingRepository<>), typeof(EventSourcingRepository<>));

            return services;
        }

        public static IServiceCollection AddEventBus(this IServiceCollection services,
            IConfiguration configuration)
        {
            var uri = configuration.GetValue<string>("rabbitmq:uri");
            var username = configuration.GetValue<string>("rabbitmq:username");
            var password = configuration.GetValue<string>("rabbitmq:password");

            services.AddMassTransit(mass =>
            {
                mass.AddBus(context => Bus.Factory.CreateUsingRabbitMq(cfg =>
                {
                    cfg.UseHealthCheck(context);

                    cfg.Host(uri, host =>
                    {
                        host.Username(username);
                        host.Password(password);
                    });
                }));
            });

            services.AddMassTransitHostedService();

            return services;
        }
    }
}
