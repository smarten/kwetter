using System.Threading.Tasks;
using Grpc.Core;
using GrpcUser;
using MediatR;
using Microsoft.Extensions.Logging;
using User.Application.EventHandling.RegisterUser;

namespace User.Web.Services
{
    public class UserGrpcService : UserGrpc.UserGrpcBase
    {
        private readonly IMediator _mediator;
        private readonly ILogger<UserGrpcService> _logger;

        public UserGrpcService(IMediator mediator, ILogger<UserGrpcService> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        public override async Task<RegisterUserResponse> RegisterUser(RegisterUserRequest request, ServerCallContext context)
        {
            _logger.LogInformation("begin grpc call from method {Method} for user with id {userId}", context.Method, request);

            var command = new RegisterUserCommand(request.User.Id, request.User.Username, request.User.Email);
            await _mediator.Send(command);

            context.Status = new Status(StatusCode.OK, "User successfully registered.");
            return new RegisterUserResponse();
        }
    }
}
