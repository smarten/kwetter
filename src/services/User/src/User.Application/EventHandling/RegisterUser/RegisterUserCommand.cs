﻿using MediatR;

namespace User.Application.EventHandling.RegisterUser
{
    public class RegisterUserCommand : IRequest
    {
        public string AuthUserId { get; }
        public string Username { get; }
        public string Email { get; }

        public RegisterUserCommand(string authUserId, string username, string email)
        {
            AuthUserId = authUserId;
            Username = username;
            Email = email;
        }
    }
}
