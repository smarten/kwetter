﻿using System.Threading;
using System.Threading.Tasks;
using MassTransit;
using MediatR;
using Microsoft.Extensions.Logging;
using User.Domain.Aggregates.User;
using User.Domain.Core;

namespace User.Application.EventHandling.RegisterUser
{
    public class RegisterUserCommandHandler : IRequestHandler<RegisterUserCommand>
    {
        private readonly IEventSourcingRepository<UserId> _eventSourcingRepository;
        private readonly ILogger<RegisterUserCommandHandler> _logger;
        private readonly IPublishEndpoint _publisher;

        public RegisterUserCommandHandler(
            IEventSourcingRepository<UserId> eventSourcingRepository,
            IPublishEndpoint publisher,
            ILogger<RegisterUserCommandHandler> logger)
        {
            _eventSourcingRepository = eventSourcingRepository;
            _publisher = publisher;
            _logger = logger;
        }

        public async Task<Unit> Handle(RegisterUserCommand request, CancellationToken cancellationToken)
        {
            var user = new Domain.Aggregates.User.User(new UserId(request.AuthUserId), request.Username, new Email(request.Email));

            _logger.LogInformation($"Preparing to register user with aggregate id {user.Id}.");
            
            await _eventSourcingRepository.SaveAsync(user);
            foreach (var @event in user.GetUncommittedEvents())
            {
                await _publisher.Publish((object) @event, cancellationToken);
            }

            return Unit.Value;
        }
    }
}
