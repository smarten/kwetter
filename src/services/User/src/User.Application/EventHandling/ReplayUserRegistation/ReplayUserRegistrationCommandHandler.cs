﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DomainDrivenDesign.Core;
using MassTransit;
using MediatR;
using Microsoft.Extensions.Logging;
using User.Application.Auth;
using User.Application.EventHandling.RegisterUser;
using User.Domain.Aggregates.User;
using User.Domain.Core;

namespace User.Application.EventHandling.ReplayUserRegistation
{
    public class ReplayUserRegistrationCommandHandler : IRequestHandler<ReplayUserRegistrationCommand>
    {
        private readonly IAuthClient _authClient;
        private readonly IPublishEndpoint _publisher;
        private readonly IEventSourcingRepository<UserId> _eventSourcingRepository;
        private readonly ILogger<ReplayUserRegistrationCommandHandler> _logger;

        public ReplayUserRegistrationCommandHandler(
            IAuthClient authClient,
            IPublishEndpoint publisher,
            IEventSourcingRepository<UserId> eventSourcingRepository,
            ILogger<ReplayUserRegistrationCommandHandler> logger)
        {
            _authClient = authClient;
            _eventSourcingRepository = eventSourcingRepository;
            _publisher = publisher;
            _logger = logger;
        }

        public async Task<Unit> Handle(ReplayUserRegistrationCommand request, CancellationToken cancellationToken)
        {
            _logger.LogInformation("Starting user registration replay.");

            // TODO: find a way to make consumers be ready before messages are published, UserRegisteredEvent messages are published before queues are declared
            await Task.Delay(120_000, cancellationToken);

            var authUsers = (await _authClient.GetAllUsersAsync()).ToList();

            if (!await VerifyIfReplayRequired(authUsers))
                return Unit.Value;

            foreach (var authUser in authUsers)
            {
                _logger.LogInformation($"Replaying registration for user with auth id {authUser.Id}...");

                var user = new Domain.Aggregates.User.User(new UserId(authUser.Id), authUser.Username, new Email(authUser.Email));

                try
                {
                    await _eventSourcingRepository.SaveAsync(user);
                }
                catch (Exception e)
                {
                    _logger.LogError($"Unable to store user registration event while replaying user registration for user {user.Id}. Exception details: {e.Message}");
                    throw;
                }

                var @event = user.GetUncommittedEvents().LastOrDefault();
                try
                {
                    await _publisher.Publish((object) @event, cancellationToken);
                }
                catch (Exception e)
                {
                    _logger.LogError($"Unable to publish event {@event?.EventId} for user {@event?.AggregateId} while replaying user registration.", e.Message);
                    throw;
                }
            }

            return Unit.Value;
        }

        private async Task<bool> VerifyIfReplayRequired(IList<IAuthUser> authUsers)
        {
            if (!authUsers.Any())
                return false;

            var userId = new UserId(authUsers.FirstOrDefault()?.Id);
            try
            { 
                await _eventSourcingRepository.ReadEventStreamAsync<Domain.Aggregates.User.User>(userId);
            }
            catch (ArgumentException)
            {
                _logger.LogInformation("Confirmed that replay for user registration is required. (One of the users was not registered in the system.)");
                return true;
            }

            _logger.LogInformation("Confirmed that replay for user registration is not required.");
            return false;
        }
    }
}
