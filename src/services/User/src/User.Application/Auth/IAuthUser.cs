﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace User.Application.Auth
{
    public interface IAuthUser
    {
        string Id { get; }
        string Username { get; }
        string Email { get; }
    }
}
