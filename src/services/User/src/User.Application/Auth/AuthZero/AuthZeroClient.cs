﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using User.Application.Auth.AuthZero.Config;
using User.Application.Auth.AuthZero.Models;

namespace User.Application.Auth.AuthZero
{
    public class AuthZeroClient : IAuthClient
    {
        private readonly HttpClient _httpClient;
        private readonly AuthZeroConfig _authZeroConfig;
        private AuthZeroAccessToken _accessToken;
        private ILogger<AuthZeroClient> _logger;

        public AuthZeroClient(HttpClient httpClient, IOptions<AuthZeroConfig> authZeroConfig, ILogger<AuthZeroClient> logger)
        {
            _httpClient = httpClient;
            _authZeroConfig = authZeroConfig.Value;
            _accessToken = null;

            _httpClient.BaseAddress = new Uri(_authZeroConfig.Domain);
            _logger = logger;
        }

        private async Task RefreshTokenAsync()
        {
            using var content = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("grant_type", "client_credentials"),
                new KeyValuePair<string, string>("client_id", _authZeroConfig.ClientId),
                new KeyValuePair<string, string>("client_secret", _authZeroConfig.ClientSecret),
                new KeyValuePair<string, string>("audience", _httpClient.BaseAddress + "api/v2/")
            });

            var response = await _httpClient.PostAsync("oauth/token", content);

            try
            {
                response.EnsureSuccessStatusCode();
            }
            catch (Exception e)
            {
                _logger.LogError("Unable to refresh auth0 token", e.Message);
                throw;
            }

            var responseContent = await response.Content.ReadAsStringAsync();
            _accessToken = JsonConvert.DeserializeObject<AuthZeroAccessToken>(responseContent);
        }

        private async Task RefreshTokenIfExpiredAsync()
        {
            if (_accessToken == null || _accessToken.IsExpired)
            {
                await RefreshTokenAsync();
            }
        }

        public async Task<IEnumerable<IAuthUser>> GetAllUsersAsync()
        {
            // TODO: Make this not sometimes throw a 403 when awaited, access token seems to be used before it is set
            RefreshTokenIfExpiredAsync().Wait();

            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _accessToken?.AccessToken);

            var response = await _httpClient.GetAsync("api/v2/users?search_engine=v3");

            try
            {
                response.EnsureSuccessStatusCode();
            }
            catch (Exception e)
            {
                _logger.LogError("Unable to retrieve all users from auth0", e.Message);
                throw;
            }

            var responseContent = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<List<AuthZeroUser>>(responseContent);
        }
    }
}
