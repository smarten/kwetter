﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace User.Application.Auth.AuthZero.Models
{
    public class AuthZeroUser : IAuthUser
    {
        [JsonProperty(PropertyName = "user_id")]
        public string Id { get; set; }
        
        [JsonProperty("username")]
        public string Username { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }
    }
}
