﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainDrivenDesign.Core;
using EventStore;
using MassTransit;
using Microsoft.Extensions.Logging;
using User.Domain.Core;

namespace User.Infrastructure.Repositories
{
    public class EventSourcingRepository<TAggregateId> : IEventSourcingRepository<TAggregateId>
    {
        private readonly IEventStore _eventStore;
        private readonly ILogger<EventSourcingRepository<TAggregateId>> _logger;

        public EventSourcingRepository(IEventStore eventStore, ILogger<EventSourcingRepository<TAggregateId>> logger)
        {
            _eventStore = eventStore;
            _logger = logger;
        }

        public Task<IEnumerable<IDomainEvent<TAggregateId>>> ReadEventStreamAsync<TAggregate>(TAggregateId aggregateId)
        {
            return _eventStore.ReadEventStreamAsync(aggregateId);
        }

        public Task SaveAsync(AggregateRoot<TAggregateId> aggregate)
        {
            var processEventTasks = new List<Task>();
            foreach (var @event in aggregate.GetUncommittedEvents())
            {
                _logger.LogInformation($"Preparing to store event {@event.EventId} with version {@event.AggregateVersion} of aggregate {@event.AggregateId}.");
                processEventTasks.Add(_eventStore.AppendEventAsync(@event));
            }

            return Task.WhenAll(processEventTasks);
        }
    }
}
