﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainDrivenDesign.Core;

namespace User.Domain.Aggregates.User
{
    public class UserId : IAggregateId
    {
        public string Id { get; private set; }

        public UserId(string id)
        {
            Id = id;
        }

        public string IdToString()
        {
            return Id;
        }

        public override string ToString()
        {
            return IdToString();
        }
    }
}
