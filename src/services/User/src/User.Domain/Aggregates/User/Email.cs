﻿using System;
using System.Globalization;
using System.Net.Mail;
using System.Text.RegularExpressions;

namespace User.Domain.Aggregates.User
{
    public sealed class Email
    {
        public string Value { get; }

        public Email(string value)
        {
            Value = new MailAddress(value).Address;
        }
    }
}
