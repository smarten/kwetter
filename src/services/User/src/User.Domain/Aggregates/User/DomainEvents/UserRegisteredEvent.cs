﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainDrivenDesign.Core;

namespace User.Domain.Aggregates.User.DomainEvents
{
    public class UserRegisteredEvent : IDomainEvent<UserId>
    {
        public string Username { get; private set; }
        public Email Email { get; private set; }
        public Guid EventId { get; private set; }
        public UserId AggregateId { get; private set; }
        public DateTime CreatedAt { get; private set; }
        public long AggregateVersion { get; private set; }

        public UserRegisteredEvent(UserId aggregateId, string username, Email email, long aggregateVersion)
        {
            AggregateId = aggregateId;
            Username = username;
            Email = email;
            EventId = Guid.NewGuid();
            CreatedAt = DateTime.Now;
            AggregateVersion = aggregateVersion;
        }
        public IDomainEvent<UserId> WithVersion(long aggregateVersion)
        {
            return new UserRegisteredEvent(AggregateId, Username, Email, aggregateVersion);
        }

    }
}
