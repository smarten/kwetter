﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainDrivenDesign.Core;
using User.Domain.Aggregates.User.DomainEvents;

namespace User.Domain.Aggregates.User
{
    public class User : AggregateRoot<UserId>
    {
        public string Username { get; }
        public Email Email { get; }

        public User(UserId id, string username, Email email) : base(id)
        {
            Username = username;
            Email = email;

            AddDomainEvent(new UserRegisteredEvent(Id, Username, Email, Version + 1));
        }
    }
}
