﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KweetPosting.Domain.Aggregates.Common;
using KweetPosting.Domain.Core;
using MassTransit;
using Microsoft.Extensions.Logging;
using User.Domain.Aggregates.User.DomainEvents;

namespace KweetPosting.Application.EventBus.Handlers
{
    public class UserRegisteredEventConsumer : IConsumer<UserRegisteredEvent>
    {
        private readonly IEventSourcingRepository<UserId> _eventSourcingRepository;
        private readonly ILogger<UserRegisteredEventConsumer> _logger;

        public UserRegisteredEventConsumer(
            IEventSourcingRepository<UserId> eventSourcingRepository,
            ILogger<UserRegisteredEventConsumer> logger)
        {
            _eventSourcingRepository = eventSourcingRepository;
            _logger = logger;
        }

        public Task Consume(ConsumeContext<UserRegisteredEvent> context)
        {
            _logger.LogInformation($"Processing UserRegisteredEvent {context.Message.EventId} for user {context.Message.AggregateId}");

            var @event = context.Message;

            var user = new Domain.Aggregates.User.User(@event.AggregateId, @event.Username);

            return _eventSourcingRepository.SaveAsync(user);
        }
    }
}
