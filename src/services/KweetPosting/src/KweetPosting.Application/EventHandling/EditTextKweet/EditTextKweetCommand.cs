﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KweetPosting.Domain.Aggregates.Common;
using MediatR;

namespace KweetPosting.Application.EventHandling.EditTextKweet
{
    public class EditTextKweetCommand : IRequest
    {
        public TextKweetId TextKweetId { get; }
        public string Text { get; }
        public UserId UserId { get; }

        public EditTextKweetCommand(TextKweetId textKweetId, string text, UserId userId)
        {
            TextKweetId = textKweetId;
            Text = text;
            UserId = userId;
        }
    }
}
