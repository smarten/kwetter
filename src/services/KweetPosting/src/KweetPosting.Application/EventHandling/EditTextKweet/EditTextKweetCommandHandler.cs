﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using KweetPosting.Domain.Aggregates.Common;
using KweetPosting.Domain.Aggregates.TextKweet;
using KweetPosting.Domain.Core;
using MassTransit;
using MediatR;
using Microsoft.Extensions.Logging;

namespace KweetPosting.Application.EventHandling.EditTextKweet
{
    public class EditTextKweetCommandHandler : IRequestHandler<EditTextKweetCommand>
    {
        private readonly ILogger<EditTextKweetCommandHandler> _logger;
        private readonly IEventSourcingRepository<TextKweetId> _eventSourcingRepository;
        private readonly IPublishEndpoint _publisher;

        public EditTextKweetCommandHandler(
            IEventSourcingRepository<TextKweetId> eventSourcingRepository,
            ILogger<EditTextKweetCommandHandler> logger,
            IPublishEndpoint publisher)
        {
            _eventSourcingRepository = eventSourcingRepository;
            _logger = logger;
            _publisher = publisher;
        }

        public async Task<Unit> Handle(EditTextKweetCommand request, CancellationToken cancellationToken)
        {
            _logger.LogInformation($"Preparing to edit text kweet {request.TextKweetId} for user {request.UserId}.");

            var currentTextKweetEvents = await _eventSourcingRepository.ReadEventStreamAsync<TextKweet>(request.TextKweetId);
            var currentTextKweet = new TextKweet(currentTextKweetEvents);

            currentTextKweet.EditText(request.Text);

            await _eventSourcingRepository.SaveAsync(currentTextKweet);
            await _publisher.Publish((object) currentTextKweet.GetUncommittedEvents().Last(), cancellationToken);

            return Unit.Value;
        }
    }
}
