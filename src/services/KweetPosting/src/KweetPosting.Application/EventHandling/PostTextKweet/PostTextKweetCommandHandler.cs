﻿using System.Threading;
using System.Threading.Tasks;
using KweetPosting.Domain.Aggregates.Common;
using KweetPosting.Domain.Aggregates.TextKweet;
using KweetPosting.Domain.Core;
using MassTransit;
using MediatR;
using Microsoft.Extensions.Logging;

namespace KweetPosting.Application.EventHandling.PostTextKweet
{
    public class PostTextKweetCommandHandler : IRequestHandler<PostTextKweetCommand>
    {
        private readonly IEventSourcingRepository<TextKweetId> _eventSourcingRepository;
        private readonly ILogger<PostTextKweetCommandHandler> _logger;
        private readonly IPublishEndpoint _publisher;

        public PostTextKweetCommandHandler(
            IEventSourcingRepository<TextKweetId> eventSourcingRepository,
            ILogger<PostTextKweetCommandHandler> logger,
            IPublishEndpoint publisher)
        {
            _eventSourcingRepository = eventSourcingRepository;
            _logger = logger;
            _publisher = publisher;
        }

        public async Task<Unit> Handle(PostTextKweetCommand request, CancellationToken cancellationToken)
        {
            _logger.LogInformation($"Preparing to post new kweet for user {request.UserId}");

            var postedTextKweet = new TextKweet(request.Text, request.UserId);

            await _eventSourcingRepository.SaveAsync(postedTextKweet);
            foreach (var @event in postedTextKweet.GetUncommittedEvents())
            {
                _logger.LogInformation(
                    $"Publishing {@event.GetType().Name} event {@event.AggregateId} to the event bus.");
                await _publisher.Publish((object) @event, cancellationToken);
            }

            return Unit.Value;
        }
    }
}
