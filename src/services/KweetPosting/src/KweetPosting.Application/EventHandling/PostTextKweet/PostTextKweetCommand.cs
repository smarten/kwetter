﻿using KweetPosting.Domain.Aggregates.Common;
using MediatR;

namespace KweetPosting.Application.EventHandling.PostTextKweet
{
    public class PostTextKweetCommand : IRequest
    {
        public UserId UserId { get; }
        public string Text { get; }

        public PostTextKweetCommand(UserId userId, string text)
        {
            UserId = userId;
            Text = text;
        }
    }
}
