﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainDrivenDesign.Core;
using KweetPosting.Domain.Aggregates.Common;
using User.Domain.Aggregates.User.DomainEvents;

namespace KweetPosting.Domain.Aggregates.User
{
    public class User : AggregateRoot<UserId>
    {
        public string Username { get; private set; }

        public User(UserId id, string username) : base(id)
        { 
            On(new UserRegisteredEvent(id, username, Version + 1));
        }

        private void On(UserRegisteredEvent @event)
        {
            Username = @event.Username;
            AddDomainEvent(@event);
        }
    }
}