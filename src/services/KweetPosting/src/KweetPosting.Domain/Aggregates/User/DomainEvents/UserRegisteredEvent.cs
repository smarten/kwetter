﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainDrivenDesign.Core;
using KweetPosting.Domain.Aggregates.Common;

// ReSharper disable once CheckNamespace
namespace User.Domain.Aggregates.User.DomainEvents
{
    public class UserRegisteredEvent : IDomainEvent<UserId>
    {
        public Guid EventId { get; private set; }
        public UserId AggregateId { get; private set; }
        public string Username { get; private set; }
        public DateTime CreatedAt { get; private set; }
        public long AggregateVersion { get; private set; }

        public UserRegisteredEvent(UserId aggregateId, string username, long aggregateVersion)
        {
            EventId = Guid.NewGuid();
            AggregateId = aggregateId;
            Username = username;
            CreatedAt = DateTime.Now;
            AggregateVersion = aggregateVersion;
        }

        public IDomainEvent<UserId> WithVersion(long aggregateVersion)
        {
            return new UserRegisteredEvent(AggregateId, Username, aggregateVersion);
        }
    }
}
