﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainDrivenDesign.Core;
using KweetPosting.Domain.Aggregates.Common;

namespace KweetPosting.Domain.Aggregates.TextKweet.DomainEvents
{
    public class TextEditedEvent : IDomainEvent<TextKweetId>
    {
        public Guid EventId { get; private set; }
        public TextKweetId AggregateId { get; private set; }
        public string Text { get; private set; }
        public UserId UserId { get; private set; }
        public DateTime CreatedAt { get; private set; }
        public long AggregateVersion { get; private set; }

        public TextEditedEvent(TextKweetId aggregateId, string text, UserId userId, long aggregateVersion)
        {
            EventId = Guid.NewGuid();
            Text = text;
            UserId = userId;
            AggregateId = aggregateId;
            CreatedAt = DateTime.Now;
            AggregateVersion = aggregateVersion;
        }

        public IDomainEvent<TextKweetId> WithVersion(long aggregateVersion)
        {
            return new TextKweetPostedEvent(AggregateId, Text, UserId, aggregateVersion);
        }
    }
}
