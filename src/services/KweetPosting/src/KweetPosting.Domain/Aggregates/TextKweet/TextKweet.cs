﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainDrivenDesign.Core;
using KweetPosting.Domain.Aggregates.Common;
using KweetPosting.Domain.Aggregates.TextKweet.DomainEvents;

namespace KweetPosting.Domain.Aggregates.TextKweet
{
    public class TextKweet : AggregateRoot<TextKweetId>
    {
        public string Text { get; private set; }
        public UserId UserId { get; private set; }

        public TextKweet(string text, UserId userId) : base(new TextKweetId())
        {
            On(new TextKweetPostedEvent(Id, text, userId, Version + 1));
        }

        public TextKweet(IEnumerable<IDomainEvent<TextKweetId>> existingEvents) : base(new TextKweetId())
        {
            foreach (var @event in existingEvents)
            {
                On((dynamic) @event);
            }
        }

        public void EditText(string text)
        {
            On(new TextEditedEvent(Id, text, UserId, Version + 1));
        }

        private void On(TextEditedEvent @event)
        {
            Text = @event.Text;

            AddDomainEvent(@event);
        }

        private void On(TextKweetPostedEvent @event)
        {
            Id = @event.AggregateId;
            Text = @event.Text;
            UserId = @event.UserId;

            AddDomainEvent(@event);
        }
    }
}
