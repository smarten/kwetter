﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainDrivenDesign.Core;

namespace KweetPosting.Domain.Aggregates.Common
{
    public class TextKweetId : IAggregateId
    {
        public Guid Id { get; private set; }

        public TextKweetId()
        {
            Id = Guid.NewGuid();
        }

        public TextKweetId(Guid id)
        {
            Id = id;
        }

        public string IdToString()
        {
            return Id.ToString();
        }

        public override string ToString()
        {
            return IdToString();
        }


    }
}
