﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DomainDrivenDesign.Core;

namespace KweetPosting.Domain.Core
{
    public interface IEventSourcingRepository<TAggregateId>
    {
        Task<IEnumerable<IDomainEvent<TAggregateId>>> ReadEventStreamAsync<TAggregate>(TAggregateId aggregateId);

        Task SaveAsync(AggregateRoot<TAggregateId> aggregate);
    }
}
