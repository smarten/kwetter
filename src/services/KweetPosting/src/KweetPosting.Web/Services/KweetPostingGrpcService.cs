﻿using System;
using System.Threading.Tasks;
using Grpc.Core;
using GrpcKweetPosting;
using KweetPosting.Application.EventHandling.EditTextKweet;
using KweetPosting.Application.EventHandling.PostTextKweet;
using KweetPosting.Domain.Aggregates.Common;
using MediatR;
using Microsoft.Extensions.Logging;

namespace KweetPosting.Web.Services
{
    public class KweetPostingGrpcService : KweetPostingGrpc.KweetPostingGrpcBase
    {
        private readonly IMediator _mediator;
        private readonly ILogger<KweetPostingGrpcService> _logger;

        public KweetPostingGrpcService(IMediator mediator, ILogger<KweetPostingGrpcService> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        public override async Task<PostTextKweetResponse> PostTextKweet(PostTextKweetRequest request, ServerCallContext context)
        {
            _logger.LogInformation("begin grpc call from method {Method} for user {userId} for posting a text kweet.", context.Method, request.UserId);

            await _mediator.Send(new PostTextKweetCommand(new UserId(request.UserId), request.Text));

            context.Status = new Status(StatusCode.OK, "Kweet succesfully saved.");
            return new PostTextKweetResponse();
        }

        public override async Task<EditTextKweetResponse> EditTextKweet(EditTextKweetRequest request, ServerCallContext context)
        {
            _logger.LogInformation(
                "begin grpc call from {Method} for user {userId} for editing a text kweet {textKweetId}",
                context.Method, request.UserId, request.TextKweetId);
            try
            {
                var textKweetGuid = Guid.Parse(request.TextKweetId);
                await _mediator.Send(new EditTextKweetCommand(new TextKweetId(textKweetGuid), request.Text,
                    new UserId(request.UserId)));
            }
            catch (FormatException)
            {
                context.Status = new Status(StatusCode.InvalidArgument,
                    $"Could not parse given textKweetId {request.TextKweetId} to valid guid.");
                return new EditTextKweetResponse();
            }

            context.Status = new Status(StatusCode.OK, "Kweet successfully edited.");
            return new EditTextKweetResponse();
        }
    }
}
