﻿using System;
using EventStore;
using EventStore.ClientAPI;
using HealthChecks.UI.Client;
using KweetPosting.Application.EventBus.Handlers;
using KweetPosting.Application.EventHandling.PostTextKweet;
using KweetPosting.Domain.Core;
using KweetPosting.Infrastructure;
using KweetPosting.Infrastructure.Repositories;
using KweetPosting.Web.Services;
using MassTransit;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using User.Domain.Aggregates.User.DomainEvents;

namespace KweetPosting.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddCustomHealthChecks(Configuration)
                .AddApplicationServices()
                .AddEventStore(Configuration)
                .AddEventBus(Configuration);

            services.AddGrpc();

            services.AddMediatR(typeof(PostTextKweetCommand));
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHealthChecks("/hc", new HealthCheckOptions
                {
                    Predicate = _ => true,
                    ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
                });

                endpoints.MapHealthChecks("/liveness", new HealthCheckOptions
                {
                    Predicate = r => r.Name.Contains("self")
                });

                endpoints.MapGrpcService<KweetPostingGrpcService>();
            });
        }
    }

    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddCustomHealthChecks(this IServiceCollection services,
            IConfiguration configuration)
        {
            services
                .AddHealthChecks()
                .AddCheck("self", () => HealthCheckResult.Healthy());

            return services;
        }

        public static IServiceCollection AddApplicationServices(this IServiceCollection services)
        {
            services.AddScoped(typeof(IEventSourcingRepository<>), typeof(EventSourcingRepository<>));

            return services;
        }

        public static IServiceCollection AddEventStore(this IServiceCollection services,
            IConfiguration configuration)
        {
            var eventStoreConnectionString = configuration.GetValue<string>("eventStoreConnectionString");
            services.AddSingleton<IEventStoreConnection>(options =>
            {
                var connection = EventStoreConnection.Create(new Uri(eventStoreConnectionString));
                connection.ConnectAsync().Wait();

                return connection;
            });

            services.AddSingleton<IEventStore>(provider =>
            {
                var eventStoreConnection = provider.GetRequiredService<IEventStoreConnection>();
                return new EventStoreEventStore(eventStoreConnection, typeof(IEventSourcingRepository<>).Assembly);
            });

            return services;
        }

        public static IServiceCollection AddEventBus(this IServiceCollection services,
            IConfiguration configuration)
        {
            var uri = configuration.GetValue<string>("rabbitmq:uri");
            var username = configuration.GetValue<string>("rabbitmq:username");
            var password = configuration.GetValue<string>("rabbitmq:password");

            services.AddMassTransit(mass =>
            {
                mass.AddConsumer<UserRegisteredEventConsumer>();

                mass.AddBus(context => Bus.Factory.CreateUsingRabbitMq(cfg =>
                {
                    cfg.UseHealthCheck(context);

                    cfg.Host(uri, host =>
                    {
                        host.Username(username);
                        host.Password(password);
                    });

                    cfg.ReceiveEndpoint("kweet-posting-user-registered", endpoint =>
                    {
                        endpoint.ConfigureConsumer<UserRegisteredEventConsumer>(context);
                    });
                }));
            });

            services.AddMassTransitHostedService();

            return services;
        }
    }
}
