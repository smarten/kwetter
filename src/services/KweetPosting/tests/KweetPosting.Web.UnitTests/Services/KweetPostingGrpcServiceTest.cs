﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using FluentAssertions;
using Grpc.Core;
using Grpc.Core.Testing;
using GrpcKweetPosting;
using KweetPosting.Web.Services;
using MediatR;
using Microsoft.Extensions.Logging.Abstractions;
using Moq;
using Xunit;

namespace KweetPosting.Web.UnitTests.Services
{
    public class KweetPostingGrpcServiceTest
    {
        [Fact]
        public async Task Should_return_valid_response_when_valid_kweet_received()
        {
            // Arrange
            var mediatorMock = new Mock<IMediator>();
            var sut = new KweetPostingGrpcService(mediatorMock.Object, new NullLogger<KweetPostingGrpcService>());

            var serverCallbackContext = TestServerCallContext.Create(
                It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<DateTime>(),
                It.IsAny<Metadata>(),
                It.IsAny<CancellationToken>(),
                It.IsAny<string>(),
                It.IsAny<AuthContext>(),
                It.IsAny<ContextPropagationToken>(),
                It.IsAny<Func<Metadata, Task>>(),
                It.IsAny<Func<WriteOptions>>(),
                It.IsAny<Action<WriteOptions>>()
            );

            var postTextKweetRequest = new PostTextKweetRequest
            {
                Text = "I posted a new kweet!",
                UserId = "123"
            };

            // Act
            var result = await sut.PostTextKweet(postTextKweetRequest, serverCallbackContext);

            // Assert
            result.Should().NotBeNull();
            serverCallbackContext.Status.StatusCode.Should().Be(StatusCode.OK);
            serverCallbackContext.Status.Detail.Should().Be("Kweet succesfully saved.");
        }
    }
}
