using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using FluentAssertions;
using Google.Protobuf.Collections;
using Grpc.Core;
using Grpc.Core.Testing;
using GrpcUser;
using Microsoft.Extensions.Logging.Abstractions;
using Moderation.ApplicationCore.Auth;
using Moderation.Web.Grpc;
using Moderation.Web.Models;
using Moq;
using Xunit;

namespace Moderation.Web.UnitTests.Grpc
{
    public class UserServiceTest
    {
        [Fact]
        public async Task Should_get_all_users_and_map_to_grpc_response()
        {
            // Arrange
            var expectedUsers = new RepeatedField<UserResponse>
            {
                new UserResponse
                {
                    Email = "johndoe@gmail.com",
                    Username = "john_doe",
                    Roles =
                    {
                        "User"
                    }
                },
                new UserResponse
                {
                    Email = "janedoe@hotmail.com",
                    Username = "jane_doe",
                    Roles =
                    {
                        "Moderator"
                    }
                }
            };

            var users = new List<AuthUserViewModel>()
            {
                new AuthUserViewModel
                {
                    Email = "johndoe@gmail.com",
                    Username = "john_doe",
                    Roles = new List<string>
                    {
                        "User"
                    }
                },
                new AuthUserViewModel
                {
                    Email = "janedoe@hotmail.com",
                    Username = "jane_doe",
                    Roles = new List<string>
                    {
                        "Moderator"
                    }
                }
            };
            var authServiceMock = new Mock<IAuthService>();
            authServiceMock.Setup(service => service.GetAllUsersAsync()).ReturnsAsync(users);

            var serverCallbackContext = TestServerCallContext.Create(
                It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<DateTime>(),
                It.IsAny<Metadata>(),
                It.IsAny<CancellationToken>(),
                It.IsAny<string>(),
                It.IsAny<AuthContext>(),
                It.IsAny<ContextPropagationToken>(),
                It.IsAny<Func<Metadata, Task>>(),
                It.IsAny<Func<WriteOptions>>(),
                It.IsAny<Action<WriteOptions>>()
            );

            var sut = new UserService(authServiceMock.Object, new NullLogger<UserService>());

            // Act
            var response = await sut.GetAllUsers(new GetAllUsersRequest(), serverCallbackContext);

            // Assert
            response.Users.Should().BeEquivalentTo(expectedUsers);
        }

        [Fact]
        public async Task Should_have_correct_status_when_all_users_are_retrieved()
        {
            // Arrange
            var authServiceMock = new Mock<IAuthService>();
            authServiceMock.Setup(service => service.GetAllUsersAsync()).ReturnsAsync(new List<IAuthUser>());

            var serverCallbackContext = TestServerCallContext.Create(
                It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<DateTime>(),
                It.IsAny<Metadata>(),
                It.IsAny<CancellationToken>(),
                It.IsAny<string>(),
                It.IsAny<AuthContext>(),
                It.IsAny<ContextPropagationToken>(),
                It.IsAny<Func<Metadata, Task>>(),
                It.IsAny<Func<WriteOptions>>(),
                It.IsAny<Action<WriteOptions>>()
            );

            var sut = new UserService(authServiceMock.Object, new NullLogger<UserService>());

            // Act
            await sut.GetAllUsers(new GetAllUsersRequest(), serverCallbackContext);

            // Assert
            serverCallbackContext.Status.StatusCode.Should().Be(StatusCode.OK);
            serverCallbackContext.Status.Detail.Should().Be("Users request succeeded.");
        }

        [Fact]
        public async Task Should_return_internal_error_when_getting_users_with_invalid_auth_provider_config()
        {
            // Arrange
            var authServiceMock = new Mock<IAuthService>();
            authServiceMock.Setup(service => service.GetAllUsersAsync()).Throws<HttpRequestException>();

            var serverCallbackContext = TestServerCallContext.Create(
                It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<DateTime>(),
                It.IsAny<Metadata>(),
                It.IsAny<CancellationToken>(),
                It.IsAny<string>(),
                It.IsAny<AuthContext>(),
                It.IsAny<ContextPropagationToken>(),
                It.IsAny<Func<Metadata, Task>>(),
                It.IsAny<Func<WriteOptions>>(),
                It.IsAny<Action<WriteOptions>>()
            );

            var sut = new UserService(authServiceMock.Object, new NullLogger<UserService>());

            // Act
            await sut.GetAllUsers(new GetAllUsersRequest(), serverCallbackContext);

            // Assert
            serverCallbackContext.Status.StatusCode.Should().Be(StatusCode.Internal);
            serverCallbackContext.Status.Detail.Should()
                .Be("Something went wrong with the authentication provider configuration.");
        }

        [Fact]
        public async Task Should_give_no_users_back_when_all_users_are_trying_to_be_gotten()
        {
            // Arrange
            var authServiceMock = new Mock<IAuthService>();
            authServiceMock.Setup(service => service.GetAllUsersAsync()).Throws<HttpRequestException>();

            var serverCallbackContext = TestServerCallContext.Create(
                It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<DateTime>(),
                It.IsAny<Metadata>(),
                It.IsAny<CancellationToken>(),
                It.IsAny<string>(),
                It.IsAny<AuthContext>(),
                It.IsAny<ContextPropagationToken>(),
                It.IsAny<Func<Metadata, Task>>(),
                It.IsAny<Func<WriteOptions>>(),
                It.IsAny<Action<WriteOptions>>()
            );

            var sut = new UserService(authServiceMock.Object, new NullLogger<UserService>());

            // Act
            var response = await sut.GetAllUsers(new GetAllUsersRequest(), serverCallbackContext);

            // Assert
            response.Should().NotBeNull();
            response.Users.Should().BeEmpty();
        }
    }
}
