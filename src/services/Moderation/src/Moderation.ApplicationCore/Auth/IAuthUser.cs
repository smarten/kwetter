﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Moderation.ApplicationCore.Auth
{
    public interface IAuthUser
    {
        [JsonProperty("email")]
        string Email { get; }

        [JsonProperty("username")]
        string Username { get; }

        [JsonProperty("roles")]
        List<string> Roles { get; }
    }
}
