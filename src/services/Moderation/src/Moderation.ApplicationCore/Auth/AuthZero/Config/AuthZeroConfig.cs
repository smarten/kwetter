﻿namespace Moderation.ApplicationCore.Auth.AuthZero.Config
{
    public class AuthZeroConfig
    {
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
        public string Domain { get; set; }
    }
}
