﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Moderation.ApplicationCore.Auth.AuthZero.Models;
using Newtonsoft.Json;
using Microsoft.Extensions.Options;
using Moderation.ApplicationCore.Auth.AuthZero.Config;

namespace Moderation.ApplicationCore.Auth.AuthZero
{
    public class AuthZeroService : IAuthService
    {
        private readonly HttpClient _httpClient;
        private readonly AuthZeroConfig _authZeroConfig;
        private AuthZeroAccessToken _accessToken;

        public AuthZeroService(HttpClient httpClient, IOptions<AuthZeroConfig> authZeroConfig)
        {
            _httpClient = httpClient;
            _authZeroConfig = authZeroConfig.Value;
            _accessToken = null;

            _httpClient.BaseAddress = new Uri(_authZeroConfig.Domain);
        }

        private async Task RefreshTokenAsync()
        {
            using var content = new FormUrlEncodedContent(new []
            {
                new KeyValuePair<string, string>("grant_type", "client_credentials"), 
                new KeyValuePair<string, string>("client_id", _authZeroConfig.ClientId),
                new KeyValuePair<string, string>("client_secret", _authZeroConfig.ClientSecret),
                new KeyValuePair<string, string>("audience", _httpClient.BaseAddress + "api/v2/") 
            });

            var response = await _httpClient.PostAsync("oauth/token", content);

            response.EnsureSuccessStatusCode();

            var responseContent = await response.Content.ReadAsStringAsync();
            _accessToken = JsonConvert.DeserializeObject<AuthZeroAccessToken>(responseContent);
        }

        private async Task RefreshTokenIfExpiredAsync()
        {
            if (_accessToken == null || _accessToken.IsExpired)
            {
                await RefreshTokenAsync();
            }            
        }

        public async Task<IEnumerable<IAuthUser>> GetAllUsersAsync()
        {
            await RefreshTokenIfExpiredAsync();

            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _accessToken?.AccessToken);
            var response = await _httpClient.GetAsync("api/v2/users?search_engine=v3");

            response.EnsureSuccessStatusCode();

            var responseContent = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<List<AuthZeroUser>>(responseContent);
        }
    }
}
