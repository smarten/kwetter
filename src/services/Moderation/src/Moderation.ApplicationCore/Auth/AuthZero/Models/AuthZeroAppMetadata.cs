﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Moderation.ApplicationCore.Auth.AuthZero.Models
{
    public class AuthZeroAppMetadata
    {
        [JsonProperty("roles")]
        public List<string> Roles { get; set; }
    }
}
