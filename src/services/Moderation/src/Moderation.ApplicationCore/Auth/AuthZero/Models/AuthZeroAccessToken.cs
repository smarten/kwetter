﻿using System;
using Newtonsoft.Json;

namespace Moderation.ApplicationCore.Auth.AuthZero.Models
{
    /// <summary>
    /// Auth0 Access Token that is used for authentication for the Auth0 API.
    /// Only used for deserializing JSON and should not be used for anything else.
    /// </summary>
    internal class AuthZeroAccessToken
    {
        private DateTime _dateTimeOfExpiry;

        [JsonProperty("access_token")]
        public string AccessToken { get; set; }

        [System.Text.Json.Serialization.JsonIgnore]
        public bool IsExpired => _dateTimeOfExpiry < DateTime.Now;

        /// <summary>
        /// Requires a value that specifies how long it takes for this token to expire. The value is used to set the <see cref="_dateTimeOfExpiry"/> field. 
        /// After setting this property, you will be able to use the <see cref="IsExpired"/> property to determine whether the token is expired or not.
        /// </summary>
        [JsonProperty("expires_in")]
        public int ExpiresIn
        {
            set => _dateTimeOfExpiry = DateTime.Now.AddSeconds(value);
        }

        [JsonProperty(PropertyName = "scope")]
        public string Scope { get; set; }

        [JsonProperty("token_type")]
        public string TokenType { get; set; }
    }
}
