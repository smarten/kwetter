﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Moderation.ApplicationCore.Auth.AuthZero.Models
{
    public class AuthZeroUser : IAuthUser
    {
        [JsonProperty(PropertyName = "email")]
        public string Email { get; set; }

        [JsonProperty(PropertyName = "email_verified")]
        public bool IsVerified { get; set; }

        [JsonProperty(PropertyName = "username")]
        public string Username { get; set; }

        [JsonProperty(PropertyName = "user_id")]
        public string UserId { get; set; }

        /// <summary>
        /// This property is solely used to deserialize received JSON and extract the user roles out of it.
        /// </summary>
        [JsonProperty(PropertyName = "app_metadata")]
        public AuthZeroAppMetadata AppMetadata
        {
            set => Roles = value?.Roles;
        }

        public List<string> Roles { get; private set; }

        [JsonProperty("blocked")]
        public bool IsBlocked { get; set; }
    }
}
