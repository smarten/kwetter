﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Moderation.ApplicationCore.Auth
{
    public interface IAuthService
    {
        Task<IEnumerable<IAuthUser>> GetAllUsersAsync();
    }
}
