﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Grpc.Core;
using GrpcUser;
using Microsoft.Extensions.Logging;
using Moderation.ApplicationCore.Auth;

namespace Moderation.Web.Grpc
{
    public class UserService : UserGrpc.UserGrpcBase
    {
        private readonly IAuthService _authService;
        private readonly ILogger<UserService> _logger;

        public UserService(IAuthService authService, ILogger<UserService> logger)
        {
            _authService = authService;
            _logger = logger;
        }

        public override async Task<GetAllUsersResponse> GetAllUsers(GetAllUsersRequest request, ServerCallContext context)
        {
            _logger.LogInformation("Begin grpc call from method {Method} for getting users", context.Method);

            try
            {
                var users = await _authService.GetAllUsersAsync();

                context.Status = new Status(StatusCode.OK, "Users request succeeded.");
                return MapToGetAllUsersResponse(users);
            }
            catch (HttpRequestException e)
            {
                _logger.LogError(e.ToString());
                context.Status = new Status(StatusCode.Internal, "Something went wrong with the authentication provider configuration.");
            }

            return new GetAllUsersResponse();
        }

        private static GetAllUsersResponse MapToGetAllUsersResponse(IEnumerable<IAuthUser> users)
        {
            var response = new GetAllUsersResponse();

            foreach (var user in users)
            {
                var userResponse = new UserResponse
                {
                    Email = user.Email,
                    Username = user.Username
                };

                foreach (var role in user.Roles)
                {
                    userResponse.Roles.Add(role);
                }

                response.Users.Add(userResponse);
            }

            return response;
        }
    }
}
