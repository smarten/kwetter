﻿using System.Collections.Generic;
using Moderation.ApplicationCore.Auth;

namespace Moderation.Web.Models
{
    public class AuthUserViewModel : IAuthUser
    {
        public string Email { get; set; }
        public string Username { get; set; }
        public List<string> Roles { get; set; }
    }
}
