﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HealthChecks.UI.Client;
using KweetReading.Application.EventBus.Handlers;
using KweetReading.Application.EventHandling.Queries.GetUserKweets;
using KweetReading.Domain.Core;
using KweetReading.Infrastructure;
using KweetReading.Web.Services;
using MassTransit;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.Extensions.Hosting;
using MongoDB.Driver;

namespace KweetReading.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddCustomHealthChecks()
                .AddEventBus(Configuration)
                .AddMongoDb(Configuration)
                .AddApplicationServices();

            services.AddGrpc();

            services.AddMediatR(typeof(GetUserKweetsQuery));
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHealthChecks("/hc", new HealthCheckOptions
                {
                    Predicate = _ => true,
                    ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
                });

                endpoints.MapHealthChecks("/liveness", new HealthCheckOptions
                {
                    Predicate = r => r.Name.Contains("self")
                });

                endpoints.MapGrpcService<KweetReadingGrpcService>();
            });
        }
    }

    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddCustomHealthChecks(this IServiceCollection services)
        {
            services
                .AddHealthChecks()
                .AddCheck("self", () => HealthCheckResult.Healthy());

            return services;
        }

        public static IServiceCollection AddApplicationServices(this IServiceCollection services)
        {
            services.AddTransient(typeof(IRepository<>), typeof(MongoDbRepository<>));
            services.AddTransient(typeof(IReadOnlyRepository<>), typeof(MongoDbRepository<>));

            return services;
        }

        public static IServiceCollection AddMongoDb(this IServiceCollection services,
            IConfiguration configuration)
        {
            var mongoUri = configuration.GetValue<string>("mongo:uri");
            var mongoDatabase = configuration.GetValue<string>("mongo:database");

            services.AddSingleton<IMongoDatabase>(provider =>
            {
                var mongoClient = new MongoClient(mongoUri);
                return mongoClient.GetDatabase(mongoDatabase);
            });

            return services;
        }

        public static IServiceCollection AddEventBus(this IServiceCollection services,
            IConfiguration configuration)
        {
            var uri = configuration.GetValue<string>("rabbitmq:uri");
            var username = configuration.GetValue<string>("rabbitmq:username");
            var password = configuration.GetValue<string>("rabbitmq:password");

            services.AddMassTransit(mass =>
            {
                mass.AddConsumer<TextKweetPostedEventConsumer>();
                mass.AddConsumer<UserRegisteredEventConsumer>();
                mass.AddConsumer<TextEditedEventConsumer>();

                mass.AddBus(context => Bus.Factory.CreateUsingRabbitMq(cfg =>
                {
                    cfg.UseHealthCheck(context);

                    cfg.Host(uri, host =>
                    {
                        host.Username(username);
                        host.Password(password);
                    });

                    cfg.ReceiveEndpoint("kweet-reading-textkweet-posted", endpoint =>
                    {
                        endpoint.ConfigureConsumer<TextKweetPostedEventConsumer>(context);
                    });

                    cfg.ReceiveEndpoint("kweet-reading-user-registered", endpoint =>
                    {
                        endpoint.ConfigureConsumer<UserRegisteredEventConsumer>(context);
                    });

                    cfg.ReceiveEndpoint("kweet-reading-text-edited", endpoint =>
                    {
                        endpoint.ConfigureConsumer<TextEditedEventConsumer>(context);
                    });
                }));
            });

            services.AddMassTransitHostedService();

            return services;
        }
    }
}
