using System.Collections.Generic;
using System.Threading.Tasks;
using Grpc.Core;
using GrpcKweetReading;
using KweetReading.Application.EventHandling.Queries.GetUserKweets;
using KweetReading.Domain.ReadModels;
using MediatR;
using Microsoft.Extensions.Logging;

namespace KweetReading.Web.Services
{
    public class KweetReadingGrpcService : KweetReadingGrpc.KweetReadingGrpcBase
    {
        private readonly IMediator _mediator;
        private readonly ILogger<KweetReadingGrpcService> _logger;

        public KweetReadingGrpcService(
            IMediator mediator,
            ILogger<KweetReadingGrpcService> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        public override async Task<GetTextKweetsFromUserResponse> GetTextKweetsFromUser(GetTextKweetsFromUserRequest request, ServerCallContext context)
        {
            _logger.LogInformation($"Begin gRPC call for getting all kweets from user {request.UserId}");

            var userKweets = await _mediator.Send(new GetUserKweetsQuery(request.UserId));
            
            context.Status = new Status(StatusCode.OK, "Getting kweets from user request succeeded.");
            return MapToGetTextKweetsFromUserResponse(userKweets);
        }

        public GetTextKweetsFromUserResponse MapToGetTextKweetsFromUserResponse(
            IEnumerable<TextKweetReadModel> textKweets)
        {
            var response = new GetTextKweetsFromUserResponse();

            foreach (var textKweet in textKweets)
            {
                var grpcTextKweet = new TextKweet
                {
                    Id = textKweet.Id,
                    Text = textKweet.Text,
                    UserId = textKweet.UserId,
                    Username = textKweet.Username
                };

                response.TextKweetsFromUser.Add(grpcTextKweet);
            }

            return response;
        }
    }
}
