﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KweetReading.Domain.Core;
using KweetReading.Domain.ReadModels;
using MassTransit;
using Microsoft.Extensions.Logging;
using User.Domain.Aggregates.User.DomainEvents;

namespace KweetReading.Application.EventBus.Handlers
{
    public class UserRegisteredEventConsumer : IConsumer<UserRegisteredEvent>
    {
        private readonly IRepository<UserReadModel> _userRepository;
        private readonly ILogger<UserRegisteredEventConsumer> _logger;

        public UserRegisteredEventConsumer(
            IRepository<UserReadModel> userRepository,
            ILogger<UserRegisteredEventConsumer> logger
            )
        {
            _userRepository = userRepository;
            _logger = logger;
        }

        public Task Consume(ConsumeContext<UserRegisteredEvent> context)
        {
            _logger.LogInformation($"Processing UserRegisteredEvent {context.Message.EventId} for user {context.Message.AggregateId}");

            var userReadModel = new UserReadModel
            {
                Id = context.Message.AggregateId.Id,
                Username = context.Message.Username
            };

            return _userRepository.InsertAsync(userReadModel);
        }
    }
}
