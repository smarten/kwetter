﻿using System.Threading.Tasks;
using KweetPosting.Domain.Aggregates.TextKweet.DomainEvents;
using KweetReading.Domain.Core;
using KweetReading.Domain.ReadModels;
using MassTransit;
using Microsoft.Extensions.Logging;

namespace KweetReading.Application.EventBus.Handlers
{
    public class TextKweetPostedEventConsumer : IConsumer<TextKweetPostedEvent>
    {
        private readonly IRepository<TextKweetReadModel> _textKweetRepository;
        private readonly IRepository<UserReadModel> _userRepository;
        private readonly ILogger<TextKweetPostedEventConsumer> _logger;

        public TextKweetPostedEventConsumer(
            IRepository<TextKweetReadModel> textKweetRepository,
            IRepository<UserReadModel> userRepository,
            ILogger<TextKweetPostedEventConsumer> logger)
        {
            _textKweetRepository = textKweetRepository;
            _userRepository = userRepository;
            _logger = logger;
        }

        public async Task Consume(ConsumeContext<TextKweetPostedEvent> context)
        {
            _logger.LogInformation($"Processing TextKweetPostedEvent {context.Message.EventId} for kweet {context.Message.AggregateId}");

            var userThatPostedKweet = await _userRepository.GetByIdAsync(context.Message.UserId.Id);

            var textKweetReadModel = new TextKweetReadModel
            {
                Id = context.Message.AggregateId.IdToString(),
                Text = context.Message.Text,
                UserId = context.Message.UserId.Id,
                Username = userThatPostedKweet.Username
            };

            await _textKweetRepository.InsertAsync(textKweetReadModel);
        }
    }
}
