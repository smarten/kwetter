﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using KweetPosting.Domain.Aggregates.TextKweet.DomainEvents;
using System.Threading.Tasks;
using KweetReading.Domain.Core;
using KweetReading.Domain.ReadModels;
using MassTransit;
using Microsoft.Extensions.Logging;

namespace KweetReading.Application.EventBus.Handlers
{
    public class TextEditedEventConsumer : IConsumer<TextEditedEvent>
    {
        private readonly IRepository<TextKweetReadModel> _textKweetRepository;
        private readonly ILogger<TextEditedEventConsumer> _logger;

        public TextEditedEventConsumer(
            IRepository<TextKweetReadModel> textKweetRepository,
            ILogger<TextEditedEventConsumer> logger)
        {
            _textKweetRepository = textKweetRepository;
            _logger = logger;
        }

        public async Task Consume(ConsumeContext<TextEditedEvent> context)
        {
            _logger.LogInformation($"Processing {context.Message.GetType().Name} event with id {context.Message.EventId} for aggregate {context.Message.AggregateId} through event consumer.");

            var textKweetToUpdate = await _textKweetRepository.GetByIdAsync(context.Message.AggregateId.IdToString());
            textKweetToUpdate.Text = context.Message.Text;

            await _textKweetRepository.UpdateAsync(textKweetToUpdate);
        }
    }
}
