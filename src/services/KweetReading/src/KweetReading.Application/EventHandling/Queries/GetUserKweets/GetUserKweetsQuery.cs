﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KweetReading.Domain.ReadModels;
using MediatR;

namespace KweetReading.Application.EventHandling.Queries.GetUserKweets
{
    public class GetUserKweetsQuery : IRequest<IEnumerable<TextKweetReadModel>>
    {
        public string UserId { get; }

        public GetUserKweetsQuery(string userId)
        {
            UserId = userId;
        }
    }
}
