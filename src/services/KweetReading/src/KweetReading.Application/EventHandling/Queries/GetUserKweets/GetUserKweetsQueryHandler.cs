﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using KweetReading.Domain.Core;
using KweetReading.Domain.ReadModels;
using MediatR;
using Microsoft.Extensions.Logging;

namespace KweetReading.Application.EventHandling.Queries.GetUserKweets
{
    public class GetUserKweetsQueryHandler : IRequestHandler<GetUserKweetsQuery, IEnumerable<TextKweetReadModel>>
    {
        private readonly IReadOnlyRepository<TextKweetReadModel> _textKweetRepository;
        private readonly ILogger<GetUserKweetsQueryHandler> _logger;

        public GetUserKweetsQueryHandler(
            IReadOnlyRepository<TextKweetReadModel> textKweetRepository,
            ILogger<GetUserKweetsQueryHandler> logger)
        {
            _textKweetRepository = textKweetRepository;
            _logger = logger;
        }

        public async Task<IEnumerable<TextKweetReadModel>> Handle(GetUserKweetsQuery request, CancellationToken cancellationToken)
        {
            _logger.LogInformation($"Handling GetUserKweets query for user {request.UserId}");

            var userKweets = await _textKweetRepository.FindAllAsync(textKweet => textKweet.UserId == request.UserId);

            return userKweets;
        }
    }
}
