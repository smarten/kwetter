﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KweetReading.Domain.Core;

namespace KweetReading.Domain.ReadModels
{
    public class TextKweetReadModel : IReadEntity
    {
        public string Id { get; set; }
        public string Text { get; set; }
        public string UserId { get; set; }
        public string Username { get; set; }
    }
}
