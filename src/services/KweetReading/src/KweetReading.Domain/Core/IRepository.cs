﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KweetReading.Domain.Core
{
    public interface IRepository<T> : IReadOnlyRepository<T>
    {
        Task InsertAsync(T entity);

        Task UpdateAsync(T entity);
    }
}
