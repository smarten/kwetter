﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KweetReading.Domain.Core
{
    public interface IReadEntity
    {
        string Id { get; }
    }
}
