﻿using System;
using DomainDrivenDesign.Core;
using KweetReading.Domain.Aggregates.Common;

// ReSharper disable once CheckNamespace
namespace KweetPosting.Domain.Aggregates.TextKweet.DomainEvents
{
    public class TextKweetPostedEvent : IDomainEvent<TextKweetId>
    {
        public Guid EventId { get; }
        public TextKweetId AggregateId { get; }
        public string Text { get; }
        public UserId UserId { get; }
        public DateTime CreatedAt { get; }
        public long AggregateVersion { get; }

        public TextKweetPostedEvent(TextKweetId aggregateId, string text, UserId userId, long aggregateVersion)
        {
            EventId = Guid.NewGuid();
            Text = text;
            UserId = userId;
            AggregateId = aggregateId;
            CreatedAt = DateTime.Now;
            AggregateVersion = aggregateVersion;
        }

        public IDomainEvent<TextKweetId> WithVersion(long aggregateVersion)
        {
            return new TextKweetPostedEvent(AggregateId, Text, UserId, aggregateVersion);
        }
    }
}
