﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainDrivenDesign.Core;
using KweetReading.Domain.Aggregates.Common;

// ReSharper disable once CheckNamespace
namespace User.Domain.Aggregates.User.DomainEvents
{
    public class UserRegisteredEvent : IDomainEvent<UserId>
    {
        public Guid EventId { get; }
        public UserId AggregateId { get; }
        public string Username { get; }
        public DateTime CreatedAt { get; }
        public long AggregateVersion { get; }

        public UserRegisteredEvent(UserId aggregateId, string username, long aggregateVersion)
        {
            EventId = Guid.NewGuid();
            AggregateId = aggregateId;
            Username = username;
            CreatedAt = DateTime.Now;
            AggregateVersion = aggregateVersion;
        }

        public IDomainEvent<UserId> WithVersion(long aggregateVersion)
        {
            return new UserRegisteredEvent(AggregateId, Username, aggregateVersion);
        }
    }
}