﻿using DomainDrivenDesign.Core;

namespace KweetReading.Domain.Aggregates.Common
{
    public class UserId : IAggregateId
    {
        public string Id { get; private set;}

        public UserId(string id)
        {
            Id = id;
        }

        public string IdToString()
        {
            return Id;
        }

        public override string ToString()
        {
            return Id;
        }
    }
}
