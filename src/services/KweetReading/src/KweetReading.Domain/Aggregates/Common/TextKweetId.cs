﻿using System;
using DomainDrivenDesign.Core;

namespace KweetReading.Domain.Aggregates.Common
{
    public class TextKweetId : IAggregateId
    {
        public Guid Id { get; private set; }

        public TextKweetId()
        {
            Id = Guid.NewGuid();
        }

        public TextKweetId(Guid id)
        {
            Id = id;
        }

        public string IdToString()
        {
            return Id.ToString();
        }

        public override string ToString()
        {
            return IdToString();
        }
    }
}
