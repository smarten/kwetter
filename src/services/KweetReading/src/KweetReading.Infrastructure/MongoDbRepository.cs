﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using KweetReading.Domain.Core;
using MongoDB.Driver;

namespace KweetReading.Infrastructure
{
    public class MongoDbRepository<T> : IRepository<T> where T : IReadEntity
    {
        private readonly IMongoDatabase _mongoDatabase;
        private readonly string _collectionName;

        public MongoDbRepository(IMongoDatabase mongoDatabase)
        {
            _mongoDatabase = mongoDatabase;
            _collectionName = typeof(T).Name;
        }

        public async Task<IEnumerable<T>> FindAllAsync(Expression<Func<T, bool>> predicate)
        {
            var cursor = await _mongoDatabase.GetCollection<T>(_collectionName)
                .FindAsync(predicate);

            return cursor.ToEnumerable();
        }

        public Task<T> GetByIdAsync(string id)
        {
            return _mongoDatabase
                .GetCollection<T>(_collectionName)
                .Find(x => x.Id == id)
                .FirstOrDefaultAsync();
        }

        public Task InsertAsync(T entity)
        {
            return _mongoDatabase
                .GetCollection<T>(_collectionName)
                .InsertOneAsync(entity);
        }

        public Task UpdateAsync(T entity)
        {
            return _mongoDatabase
                .GetCollection<T>(_collectionName)
                .ReplaceOneAsync(o => o.Id == entity.Id, entity);
        }
    }
}
