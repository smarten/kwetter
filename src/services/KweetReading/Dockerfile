FROM mcr.microsoft.com/dotnet/core/sdk:3.1-buster AS build-env
WORKDIR /src

COPY services/KweetReading/src/KweetReading.Web/KweetReading.Web.csproj                         services/KweetReading/src/KweetReading.Web/KweetReading.Web.csproj
COPY services/KweetReading/src/KweetReading.Application/KweetReading.Application.csproj         services/KweetReading/src/KweetReading.Application/KweetReading.Application.csproj
COPY services/KweetReading/src/KweetReading.Infrastructure/KweetReading.Infrastructure.csproj   services/KweetReading/src/KweetReading.Infrastructure/KweetReading.Infrastructure.csproj
COPY services/KweetReading/src/KweetReading.Domain/KweetReading.Domain.csproj                   services/KweetReading/src/KweetReading.Domain/KweetReading.Domain.csproj

COPY shared/DomainDrivenDesign/src/DomainDrivenDesign/DomainDrivenDesign.csproj                 shared/DomainDrivenDesign/src/DomainDrivenDesign/DomainDrivenDesign.csproj
COPY shared/EventStore/src/EventStore/EventStore.csproj                                         shared/EventStore/src/EventStore/EventStore.csproj

RUN dotnet restore services/KweetReading/src/KweetReading.Web/KweetReading.Web.csproj

COPY services/KweetReading        services/KweetReading
COPY shared/DomainDrivenDesign    shared/DomainDrivenDesign
COPY shared/EventStore            shared/EventStore

RUN dotnet publish services/KweetReading/src/KweetReading.Web/KweetReading.Web.csproj -c Release -o out --no-restore

FROM mcr.microsoft.com/dotnet/core/aspnet:3.1
WORKDIR /app
COPY --from=build-env /src/out .
EXPOSE 80
ENTRYPOINT ["dotnet", "KweetReading.Web.dll"]