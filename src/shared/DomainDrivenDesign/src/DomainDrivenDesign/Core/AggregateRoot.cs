﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DomainDrivenDesign.Core
{
    public abstract class AggregateRoot<TIdentity>
    {
        private readonly List<IDomainEvent<TIdentity>> _domainEvents;

        protected AggregateRoot(TIdentity id)
        {
            Id = id;
            _domainEvents = new List<IDomainEvent<TIdentity>>();
            Version = -2;
        }

        public TIdentity Id { get; protected set; }
        public long Version { get; private set; }

        public IReadOnlyList<IDomainEvent<TIdentity>> GetUncommittedEvents() => _domainEvents.AsReadOnly();

        public void AddDomainEvent(IDomainEvent<TIdentity> @event)
        {
            if (@event.AggregateVersion == Version + 1)
            {
                Version = @event.AggregateVersion;
                _domainEvents.Add(@event);
            }
            else
            {
                throw new InvalidOperationException(
                    $"Tried to add domain event with invalid version {@event.AggregateVersion} (current version is {Version}).");
            }
        }
    }
}
