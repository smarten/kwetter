﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainDrivenDesign.Core
{
    public interface IAggregateId
    {
        string IdToString();
    }
}
