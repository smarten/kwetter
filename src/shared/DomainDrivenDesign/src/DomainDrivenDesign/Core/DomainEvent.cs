﻿using System;

namespace DomainDrivenDesign.Core
{
    //public abstract class DomainEvent
    //{
    //    public Guid EventId { get; }
    //    public Guid AggregateId { get; }
    //    public DateTime CreatedAt { get; }
    //    public long AggregateVersion { get; }

    //    protected DomainEvent(Guid aggregateId,  long aggregateVersion)
    //    {
    //        EventId = Guid.NewGuid();
    //        CreatedAt = DateTime.Now;
    //        AggregateId = aggregateId;
    //        AggregateVersion = aggregateVersion;
    //    }
    //}

    public interface IDomainEvent<out TAggregateId>
    {
        Guid EventId { get; }
        TAggregateId AggregateId { get; }
        DateTime CreatedAt { get; }
        long AggregateVersion { get; }

        IDomainEvent<TAggregateId> WithVersion(long aggregateVersion);
    }
}
