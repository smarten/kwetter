﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using DomainDrivenDesign.Core;
using EventStore.ClientAPI;
using Newtonsoft.Json;

namespace EventStore
{
    public class EventStoreEventStore : IEventStore
    {
        private readonly IEventStoreConnection _connection;
        private readonly Assembly[] _deserializableTypesAssemblies;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="eventStoreConnection">The connection from the EventStore.ClientAPI library.</param>
        /// <param name="deserializableTypesAssemblies">Assemblies with types to deserialize to when retrieving events from the event store.</param>
        public EventStoreEventStore(IEventStoreConnection eventStoreConnection, params Assembly[] deserializableTypesAssemblies)
        {
            _connection = eventStoreConnection;
            _deserializableTypesAssemblies = deserializableTypesAssemblies;
        }

        public async Task<IEnumerable<IDomainEvent<TIdentity>>> ReadEventStreamAsync<TIdentity>(TIdentity aggregateId)
        {
            var streamEvents = new List<IDomainEvent<TIdentity>>();
            StreamEventsSlice currentSlice;
            long nextSliceStart = StreamPosition.Start;

            do
            {
                currentSlice =
                    await _connection.ReadStreamEventsForwardAsync(aggregateId.ToString(), nextSliceStart, 200, false);
                if (currentSlice.Status != SliceReadStatus.Success)
                {
                    throw new ArgumentException($"Unable to find stream for {aggregateId.ToString()}.");
                }

                nextSliceStart = currentSlice.NextEventNumber;
                foreach (var resolvedEvent in currentSlice.Events)
                {
                    streamEvents.Add(Deserialize<TIdentity>(resolvedEvent.Event.EventType, resolvedEvent.Event.Data));
                }
            } 
            while (!currentSlice.IsEndOfStream);

            return streamEvents;
        }

        public async Task AppendEventAsync<TIdentity>(IDomainEvent<TIdentity> @event)
        {
            var serializedEvent = Serialize(@event);

            var eventData = new EventData(
                @event.EventId,
                @event.GetType().Name,
                true,
                serializedEvent,
                null
            );

            await _connection.AppendToStreamAsync(@event.AggregateId.ToString(), @event.AggregateVersion, eventData);
        }

        private static byte[] Serialize<TIdentity>(IDomainEvent<TIdentity> @event)
        {
            var json = JsonConvert.SerializeObject(@event);
            return Encoding.UTF8.GetBytes(json);
        }

        private IDomainEvent<TIdentity> Deserialize<TIdentity>(string eventType, byte[] data)
        {
            Type typeToDeserializeTo = null;
            foreach (var assembly in _deserializableTypesAssemblies)
            {

                typeToDeserializeTo = assembly.GetTypes().SingleOrDefault(t => t.Name == eventType);
                if (typeToDeserializeTo != null)
                {
                    break;
                }
            }

            if (typeToDeserializeTo == null)
            {
                var passedAssemblies = string.Join(", \n", _deserializableTypesAssemblies.Select(a => a.FullName));
                throw new ArgumentException($"Could not resolve given type {eventType} with the passed assemblies: {passedAssemblies}.");
            }

            var settings = new JsonSerializerSettings { ContractResolver = new PrivateSetterContractResolver() };
            Console.WriteLine(Encoding.UTF8.GetString(data));
            return (IDomainEvent<TIdentity>) JsonConvert.DeserializeObject(Encoding.UTF8.GetString(data), typeToDeserializeTo, settings);
        }
    }
}
