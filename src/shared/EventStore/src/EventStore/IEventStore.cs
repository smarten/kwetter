﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DomainDrivenDesign.Core;

namespace EventStore
{
    public interface IEventStore
    {
        Task<IEnumerable<IDomainEvent<TIdentity>>> ReadEventStreamAsync<TIdentity>(TIdentity aggregateId);

        Task AppendEventAsync<TIdentity>(IDomainEvent<TIdentity> @event);
    }
}
