(function(window) {
  window.env = window['env'] || {};

  // Environment variables
  window.env.authZeroDomain = 'marstan.auth0.com';
  window.env.authZeroEndpoint = 'https://marstan.auth0.com/';
  window.env.authZeroClientId = '5GTKG6xurZhl53NYjy6WDcyZyFnvGX2j';
  window.env.authZeroRoleNamespace = 'https://schemas.marstan.net/roles';
  window.env.authZeroAudience = 'https://api.kwetter.marstan.net';
  window.env.gatewayUrl = 'https://localhost:5001';
})(this);