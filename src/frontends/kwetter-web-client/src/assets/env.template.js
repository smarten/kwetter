(function(window) {
  window.env = window.env || {};

  // Environment variables
  window.env.authZeroDomain = '${AUTH_ZERO_DOMAIN}';
  window.env.authZeroEndpoint = '${AUTH_ZERO_ENDPOINT}';
  window.env.authZeroClientId = '${AUTH_ZERO_CLIENT_ID}';
  window.env.authZeroRoleNamespace = '${AUTH_ZERO_ROLE_NAMESPACE}';
  window.env.authZeroAudience = '${AUTH_ZERO_AUDIENCE}';
  window.env.gatewayUrl = '${GATEWAY_URL}'
})(this);