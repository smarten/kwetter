export const environment = {
  production: true,
  authZeroDomain: (window as any).env.authZeroDomain,
  authZeroEndpoint: (window as any).env.authZeroEndpoint,
  authZeroClientId: (window as any).env.authZeroClientId,
  authZeroRoleNamespace: (window as any).env.authZeroRoleNamespace,
  authZeroAudience: (window as any).env.authZeroAudience,
  gatewayUrl: (window as any).env.gatewayUrl
};
