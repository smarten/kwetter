import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthUser } from '../../models/auth-user.model';
import { environment } from 'src/environments/environment';
import { TextKweet } from '../../models/text-kweet.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private baseUrl: string;

  constructor(private http: HttpClient) {
    this.baseUrl = environment.gatewayUrl;
  }

  getUsers(): Observable<AuthUser[]> {
    return this.http.get<AuthUser[]>(this.baseUrl + '/users');
  }

  getUserKweets(userId: string): Observable<TextKweet[]> {
    return this.http.get<TextKweet[]>(this.baseUrl + `/users/kweets/${userId}`);
  }
}
