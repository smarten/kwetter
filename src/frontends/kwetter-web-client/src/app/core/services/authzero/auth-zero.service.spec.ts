import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AuthZeroService } from './auth-zero.service';

describe('AuthZeroService', () => {
  let service: AuthZeroService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ]
    });
    service = TestBed.inject(AuthZeroService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
