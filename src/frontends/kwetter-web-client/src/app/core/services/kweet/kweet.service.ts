import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { TextKweet } from '../../models/text-kweet.model';

@Injectable({
  providedIn: 'root'
})
export class KweetService {

  private baseUrl: string;

  constructor(private http: HttpClient) {
    this.baseUrl = environment.gatewayUrl;
  }

  postTextKweet(kweet: TextKweet) {
    return this.http.post<TextKweet>(this.baseUrl + '/kweet', JSON.stringify(kweet));
  }

  editTextKweet(kweet: TextKweet) {
    return this.http.put<TextKweet>(this.baseUrl + '/kweet', JSON.stringify(kweet));
  }
}
