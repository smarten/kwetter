import { TestBed } from '@angular/core/testing';

import { ModeratorGuard } from './moderator.guard';
import { RouterTestingModule } from '@angular/router/testing';

describe('ModeratorGuard', () => {
  let guard: ModeratorGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule]
    });
    guard = TestBed.inject(ModeratorGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
