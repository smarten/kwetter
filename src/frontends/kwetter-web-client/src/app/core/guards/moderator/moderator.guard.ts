import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthZeroService } from '../../services/authzero/auth-zero.service';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ModeratorGuard implements CanActivate {

  constructor(
    private authService: AuthZeroService,
    private router: Router
  ) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      return this.authService.isAuthenticated$.pipe(
        tap(async loggedIn => {
          if (!loggedIn) {
            this.authService.login(state.url);
          } else if (!await this.authService.isModerator()) {
            this.router.navigate(['/']);
          } else {
            return true;
          }
        })
      );
  }
}
