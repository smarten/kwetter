import { TestBed } from '@angular/core/testing';

import { LandingGuard } from './landing.guard';
import { RouterTestingModule } from '@angular/router/testing';

describe('LandingGuard', () => {
  let guard: LandingGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ]
    });
    guard = TestBed.inject(LandingGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
