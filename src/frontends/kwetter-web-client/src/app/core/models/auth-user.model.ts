export interface AuthUser {
  email?: string;
  username?: string;
  roles?: string[];
}
