import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { ProfileModule } from './features/profile/profile.module';
import { CoreModule } from './core/core.module';
import { ModerationModule } from './features/moderation/moderation.module';
import { LandingModule } from './features/landing/landing.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    AppRoutingModule,
    SharedModule,
    CoreModule,
    ProfileModule,
    ModerationModule,
    LandingModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
