import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/core/services/moderator/user.service';
import { AuthUser } from 'src/app/core/models/auth-user.model';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-moderation',
  templateUrl: './moderation.component.html',
  styleUrls: ['./moderation.component.scss']
})
export class ModerationComponent implements OnInit {

  users: Observable<AuthUser[]>;

  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.users = this.userService.getUsers();
  }
}
