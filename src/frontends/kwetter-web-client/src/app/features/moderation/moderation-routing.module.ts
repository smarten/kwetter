import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ModerationComponent } from './pages/moderation/moderation.component';
import { AuthGuard } from 'src/app/core/guards/auth/auth.guard';

const routes: Routes = [
  {
    path: '',
    component: ModerationComponent,
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ModerationRoutingModule { }
