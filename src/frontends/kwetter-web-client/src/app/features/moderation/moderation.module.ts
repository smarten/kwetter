import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ModerationRoutingModule } from './moderation-routing.module';
import { ModerationComponent } from './pages/moderation/moderation.component';
import { RolePipe } from 'src/app/shared/pipes/role/role.pipe';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [ModerationComponent],
  imports: [
    ModerationRoutingModule,
    SharedModule
  ],
  providers: [
    RolePipe
  ]
})
export class ModerationModule { }
