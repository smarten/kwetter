import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './profile-routing.module';
import { ProfileComponent } from './profile.component';
import { KweetBoxComponent } from '../../shared/components/kweet-box/kweet-box.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { PostedKweetListComponent } from './components/posted-kweet-list/posted-kweet-list.component';
import { PostedTextKweetBoxComponent } from './components/posted-text-kweet-box/posted-text-kweet-box.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';


@NgModule({
  declarations: [ProfileComponent, PostedKweetListComponent, PostedTextKweetBoxComponent],
  imports: [
    SharedModule,
    HomeRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class ProfileModule { }
