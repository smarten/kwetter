import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/core/services/moderator/user.service';
import { Observable } from 'rxjs';
import { TextKweet } from 'src/app/core/models/text-kweet.model';
import { AuthZeroService } from 'src/app/core/services/authzero/auth-zero.service';

@Component({
  selector: 'app-posted-kweet-list',
  templateUrl: './posted-kweet-list.component.html',
  styleUrls: ['./posted-kweet-list.component.scss']
})
export class PostedKweetListComponent implements OnInit {

  userKweets: TextKweet[] = [];

  constructor(private userService: UserService, private authService: AuthZeroService) { }

  async ngOnInit(): Promise<void> {
    const token = await this.authService.getUser$().toPromise();
    this.userService.getUserKweets(token.sub).subscribe(res => this.userKweets = res);
  }
}
