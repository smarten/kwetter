import { Component, OnInit, Input } from '@angular/core';
import { TextKweet } from 'src/app/core/models/text-kweet.model';
import { AuthZeroService } from 'src/app/core/services/authzero/auth-zero.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { KweetService } from 'src/app/core/services/kweet/kweet.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-posted-text-kweet-box',
  templateUrl: './posted-text-kweet-box.component.html',
  styleUrls: ['./posted-text-kweet-box.component.scss']
})
export class PostedTextKweetBoxComponent implements OnInit {

  @Input()
  textKweet: TextKweet;

  editKweetForm = new FormGroup({
    text: new FormControl('', [Validators.required])
  });

  loggedInUserId: string;
  isBeingEdited = false;

  get isPostedByCurrentUser() {
    return this.loggedInUserId === this.textKweet.userId;
  }

  constructor(
    private authService: AuthZeroService,
    private kweetService: KweetService,
    private toastr: ToastrService
    ) {}

  ngOnInit(): void {
    this.authService.getUser$().subscribe(res => this.loggedInUserId = res.sub);

    if (this.textKweet) {
      this.editKweetForm.controls.text.setValue(this.textKweet.text);
    }
  }

  saveEditedKweet() {
    const editedTextKweet: TextKweet = this.editKweetForm.value;
    editedTextKweet.id = this.textKweet.id;
    editedTextKweet.userId = this.textKweet.userId;

    this.kweetService.editTextKweet(editedTextKweet).subscribe(
      res => {
        this.toastr.success('Kweet succesfully edited!');
        this.isBeingEdited = false;
        this.editKweetForm.controls.text.setValue(editedTextKweet.text);
      },
      error => this.toastr.error('Something went wrong while editing the kweet.')
    );
  }
}
