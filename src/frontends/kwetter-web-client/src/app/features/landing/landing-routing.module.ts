import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LandingComponent } from './pages/landing/landing.component';
import { LandingGuard } from 'src/app/core/guards/landing/landing.guard';


const routes: Routes = [
  {
    path: '',
    component: LandingComponent,
    canActivate: [LandingGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LandingRoutingModule { }
