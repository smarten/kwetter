import { Component, OnInit } from '@angular/core';
import { AuthZeroService } from 'src/app/core/services/authzero/auth-zero.service';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements OnInit {

  constructor(private authService: AuthZeroService) { }

  ngOnInit(): void {
  }

  logIn() {
    this.authService.login();
  }

  signUp() {
    this.authService.signup();
  }
}
