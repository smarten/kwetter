import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './core/guards/auth/auth.guard';
import { ModeratorGuard } from './core/guards/moderator/moderator.guard';


const routes: Routes = [
  {
    path: 'landing',
    loadChildren: () => import('./features/landing/landing-routing.module').then(m => m.LandingRoutingModule)
  },
  {
    path: 'profile',
    loadChildren: () => import('./features/profile/profile.module').then(m => m.ProfileModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'moderation',
    loadChildren: () => import('./features/moderation/moderation.module').then(m => m.ModerationModule),
    canActivate: [ModeratorGuard]
  },
  {
    path: '**',
    redirectTo: 'landing',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
