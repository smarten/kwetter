import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { TextKweet } from 'src/app/core/models/text-kweet.model';
import { KweetService } from 'src/app/core/services/kweet/kweet.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-kweet-box',
  templateUrl: './kweet-box.component.html',
  styleUrls: ['./kweet-box.component.scss']
})
export class KweetBoxComponent implements OnInit {

  form = new FormGroup({
    text: new FormControl('', [Validators.required])
  });

  constructor(private kweetService: KweetService, private toastr: ToastrService) { }

  ngOnInit(): void {

  }

  postTextKweet(): void {
    const textKweet: TextKweet = this.form.value;
    this.kweetService.postTextKweet(textKweet).subscribe(
      res => this.toastr.success('Kweet succesfully posted!'),
      error => this.toastr.error('Something went wrong while posting the Kweet.'));
  }
}
