import { Component, OnInit } from '@angular/core';
import { AuthZeroService } from 'src/app/core/services/authzero/auth-zero.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  isModerator: boolean = undefined;

  constructor(private authService: AuthZeroService) { }

  async ngOnInit(): Promise<void> {
    this.isModerator = await this.authService.isModerator();
  }

  get isLoggedIn() {
    return this.authService.loggedIn;
  }

  logIn() {
    this.authService.login();
  }

  logOut() {
    this.authService.logout();
  }
}
