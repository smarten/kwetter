import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavbarComponent } from './navbar.component';
import { RouterTestingModule } from '@angular/router/testing';
import { AuthZeroService } from 'src/app/core/services/authzero/auth-zero.service';

describe('NavbarComponent', () => {
  let component: NavbarComponent;
  let fixture: ComponentFixture<NavbarComponent>;
  let authZeroService: AuthZeroService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavbarComponent ],
      imports: [
        RouterTestingModule
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavbarComponent);
    component = fixture.componentInstance;

    authZeroService = TestBed.inject(AuthZeroService);

    // Fake these methods because they are called in the constructor directly
    spyOn(authZeroService, 'handleAuthCallback').and.callFake(() => {});
    spyOn(authZeroService, 'localAuthSetup').and.callFake(() => {});

    fixture.detectChanges();
  });

  it('should have undefined moderator state when not initialized', () => {
    expect(component.isModerator).toBe(undefined);
  });

  it('should have moderator state when authService says user is moderator', async () => {
    spyOn(authZeroService, 'isModerator').and.returnValue(Promise.resolve(true));

    await component.ngOnInit();
    expect(component.isModerator).toBe(true);
  });

  it('should not have moderator state when authService says user is not moderator', async () => {
    spyOn(authZeroService, 'isModerator').and.returnValue(Promise.resolve(false));

    await component.ngOnInit();
    expect(component.isModerator).toBe(false);
  });

  it('should make auth service decide whether user is logged in', () => {
    authZeroService.loggedIn = true;
    expect(component.isLoggedIn).toBe(true);

    authZeroService.loggedIn = false;
    expect(component.isLoggedIn).toBe(false);
  });

  it('should log in through auth service', () => {
    spyOn(authZeroService, 'login').and.callFake(() => {});

    component.logIn();

    expect(authZeroService.login).toHaveBeenCalled();
  });

  it('should log out through auth service', () => {
    spyOn(authZeroService, 'logout').and.callFake(() => {});

    component.logOut();

    expect(authZeroService.logout).toHaveBeenCalled();
  });
});
