import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'role'
})
export class RolePipe implements PipeTransform {

  transform(roles: string[]): string {
    if (roles.includes('moderator')) {
      return 'moderator';
    } else if (roles.includes('user')) {
      return 'user';
    } else {
      return 'unknown';
    }
  }

}
