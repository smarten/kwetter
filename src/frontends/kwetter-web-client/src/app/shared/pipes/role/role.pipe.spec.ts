import { RolePipe } from './role.pipe';

describe('RolePipe', () => {
  it('should show user as moderator when roles contains moderator', () => {
    const pipe = new RolePipe();
    const roles = ['moderator'];

    const result = pipe.transform(roles);

    expect(result).toBe('moderator');
  });

  it('should show user as moderator when user has two roles, user and moderator', () => {
    const pipe = new RolePipe();
    const roles = ['user', 'moderator'];

    const result = pipe.transform(roles);

    expect(result).toBe('moderator');
  });

  it('should show user as user when when roles contains user', () => {
    const pipe = new RolePipe();
    const roles = ['user'];

    const result = pipe.transform(roles);

    expect(result).toBe('user');
  });

  it('should show user as unknown when user has no roles', () => {
    const pipe = new RolePipe();
    const roles = [];

    const result = pipe.transform(roles);

    expect(result).toBe('unknown');
  });

  it('should show user as unknown when user has unknown roles', () => {
    const pipe = new RolePipe();
    const roles = ['unknown role'];

    const result = pipe.transform(roles);

    expect(result).toBe('unknown');
  });
});
