import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './components/navbar/navbar.component';
import { FooterComponent } from './components/footer/footer.component';
import { RouterModule } from '@angular/router';
import { RolePipe } from './pipes/role/role.pipe';
import { KweetBoxComponent } from './components/kweet-box/kweet-box.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    NavbarComponent,
    FooterComponent,
    RolePipe,
    KweetBoxComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    FormsModule,
  ],
  exports: [
    CommonModule,
    NavbarComponent,
    FooterComponent,
    RolePipe,
    KweetBoxComponent
  ]
})
export class SharedModule { }
