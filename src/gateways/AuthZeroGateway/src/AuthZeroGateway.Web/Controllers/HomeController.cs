﻿using Microsoft.AspNetCore.Mvc;

namespace AuthZeroGateway.Web.Controllers
{
    [Route("")]
    public class HomeController : Controller
    {
        [HttpGet]
        public IActionResult Index()
        {
            return new RedirectResult("~/swagger");
        }
    }
}
