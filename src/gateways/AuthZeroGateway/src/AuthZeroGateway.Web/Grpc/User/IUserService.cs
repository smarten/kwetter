﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AuthZeroGateway.Web.Models;

namespace AuthZeroGateway.Web.Grpc.User
{
    public interface IUserService
    {
        Task CreateUser(UserViewModel viewModel);
    }
}
