﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AuthZeroGateway.Web.Models;
using Grpc.Core;
using GrpcUser;
using Microsoft.Extensions.Logging;

namespace AuthZeroGateway.Web.Grpc.User
{
    public class UserService : IUserService
    {
        private readonly UserGrpc.UserGrpcClient _client;
        private readonly ILogger<UserService> _logger;

        public UserService(
            UserGrpc.UserGrpcClient client,
            ILogger<UserService> logger)
        {
            _client = client;
            _logger = logger;
        }

        public async Task CreateUser(UserViewModel viewModel)
        {
            _logger.LogInformation($"Attempting to send user registration request to user service for user with id {viewModel.Id}");

            try
            {
                await _client.RegisterUserAsync(MapToRegisterUserRequest(viewModel));
            }
            catch (RpcException e)
            {
                _logger.LogError(e.Message);
                throw;
            }
        }

        private static RegisterUserRequest MapToRegisterUserRequest(UserViewModel viewModel)
        {
            return new RegisterUserRequest
            {
                User = new GrpcUser.User 
                {
                    Email = viewModel.Email,
                    Id = viewModel.Id,
                    Username = viewModel.Username
                }
            };
        }
    }
}
