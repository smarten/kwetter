using System;
using System.Net;
using System.Net.Http;
using System.Net.Security;
using System.Reflection;
using System.Security.Claims;
using GrpcKweetPosting;
using GrpcKweetReading;
using GrpcUser;
using HealthChecks.UI.Client;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using WebClientGateway.Web.Config;
using WebClientGateway.Web.Grpc;
using WebClientGateway.Web.Grpc.KweetPosting;
using WebClientGateway.Web.Grpc.KweetReading;
using WebClientGateway.Web.Grpc.User;

namespace WebClientGateway.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services
                .AddCustomOptions(Configuration)
                .AddApplicationServices()
                .AddGrpcClients(Configuration)
                .AddCustomDefaultCors(Configuration)
                .AddCustomAuthentication(Configuration)
                .AddCustomHealthChecks(Configuration);

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "Kwetter Web Client", 
                    Version = "v1"
                });
            });

            services.AddRouting(routing => routing.LowercaseUrls = true);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseSwagger().UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Kwetter Web Client API v1");
            });

            app.UseRouting();
            app.UseHttpsRedirection();

            app.UseCors();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHealthChecks("/hc", new HealthCheckOptions
                {
                    Predicate = _ => true,
                    ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
                });

                endpoints.MapHealthChecks("/liveness", new HealthCheckOptions
                {
                    Predicate = r => r.Name.Contains("self")
                });

                endpoints.MapControllers();
            });
        }
    }

    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddCustomOptions(this IServiceCollection services,
            IConfiguration configuration)
        {
            services.AddOptions();

            services.Configure<UrlsConfig>(configuration.GetSection("urls"));

            return services;
        }

        public static IServiceCollection AddCustomHealthChecks(this IServiceCollection services,
            IConfiguration configuration)
        {
            services.AddHealthChecks()
                .AddCheck("self", () => HealthCheckResult.Healthy())
                .AddUrlGroup(new Uri(configuration["moderatorHc"]), name: "moderator-service-hc", tags: new[] {"moderator-service"})
                .AddUrlGroup(new Uri(configuration["kweetPostingHc"]), name: "kweet-posting-service-hc", tags: new[] {"kweet-posting-service"})
                .AddUrlGroup(new Uri(configuration["kweetReadingHc"]), name: "kweet-reading-service-hc", tags: new[] {"kweet-reading-service"});

            return services;
        }

        public static IServiceCollection AddApplicationServices(this IServiceCollection services)
        {
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IKweetPostingService, KweetPostingService>();
            services.AddScoped<IKweetReadingService, KweetReadingService>();

            return services;
        }

        public static IServiceCollection AddGrpcClients(this IServiceCollection services,
            IConfiguration configuration)
        {
            AppContext.SetSwitch("System.Net.Http.SocketsHttpHandler.Http2UnencryptedSupport", true);
            AppContext.SetSwitch("System.Net.Http.SocketsHttpHandler.Http2Support", true);

            var urls = new UrlsConfig();
            configuration.GetSection("urls").Bind(urls);

            services.AddGrpcClient<UserGrpc.UserGrpcClient>(client =>
            {
                client.Address = new Uri(urls.GrpcModerator);
            });

            services.AddGrpcClient<KweetPostingGrpc.KweetPostingGrpcClient>(client =>
            {
                client.Address = new Uri(urls.GrpcKweetPosting);
            });

            services.AddGrpcClient<KweetReadingGrpc.KweetReadingGrpcClient>(client =>
            {
                client.Address = new Uri(urls.GrpcKweetReading);
            });

            return services;
        }

        public static IServiceCollection AddCustomAuthentication(this IServiceCollection services,
            IConfiguration configuration)
        {
            var authZeroDomain = configuration.GetValue<string>("authZeroDomain") + "/";
            var authZeroAudience = configuration.GetValue<string>("authZeroAudience");

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;

            }).AddJwtBearer(options =>
            {
                options.Authority = authZeroDomain;
                options.Audience = authZeroAudience;
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    NameClaimType = "Roles",
                    RoleClaimType = configuration.GetValue<string>("authZeroRoleNamespace")
                };
            });

            return services;
        }

        public static IServiceCollection AddCustomDefaultCors(this IServiceCollection services, 
            IConfiguration configuration)
        {
            var kwetterWebClientUrl = configuration.GetValue<string>("urls:kwetterWebClient");

            services.AddCors(options =>
                options.AddDefaultPolicy(policy =>
                {
                    policy
                        .WithOrigins(kwetterWebClientUrl)
                        .AllowAnyHeader()
                        .AllowAnyMethod()
                        .AllowCredentials();
                })
            );

            return services;
        }
    }
}
