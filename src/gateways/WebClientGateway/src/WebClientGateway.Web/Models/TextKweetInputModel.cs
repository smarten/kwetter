﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebClientGateway.Web.Models
{
    public class TextKweetInputModel
    {
        public string Text { get; set; }
    }
}
