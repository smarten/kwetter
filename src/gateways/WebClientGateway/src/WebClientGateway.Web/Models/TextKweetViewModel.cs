﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebClientGateway.Web.Models
{
    public class TextKweetViewModel
    {
        public string Id { get; set; }
        public string Text { get; set; }
        public string UserId { get; set; }
        public string Username { get; set; }
    }
}
