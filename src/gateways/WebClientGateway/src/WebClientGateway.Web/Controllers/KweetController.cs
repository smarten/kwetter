﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebClientGateway.Web.Grpc;
using WebClientGateway.Web.Grpc.KweetPosting;
using WebClientGateway.Web.Models;

namespace WebClientGateway.Web.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class KweetController : ControllerBase
    {
        private readonly IKweetPostingService _kweetService;
        private readonly ILogger<KweetController> _logger;

        public KweetController(IKweetPostingService kweetService, ILogger<KweetController> logger)
        {
            _kweetService = kweetService;
            _logger = logger;
        }

        [HttpPost]
        [Authorize(Roles = "user")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<IActionResult> PostKweet(TextKweetInputModel inputModel)
        {
            var textKweet = new TextKweetViewModel
            {
                Text = inputModel.Text,
                UserId = User.FindFirst(ClaimTypes.NameIdentifier).Value
            };

            try
            {
                await _kweetService.PostTextKweetAsync(textKweet);
            }
            catch (Exception e)
            {
                _logger.LogError("Something went wrong with the kweet service while posting a kweet", e.Message);
                return StatusCode(500);
            }

            return NoContent();
        }

        [HttpPut]
        [Authorize(Roles = "user")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        public async Task<IActionResult> EditKweet(TextKweetViewModel textKweet)
        {
            var currentUser = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            if (currentUser != textKweet.UserId)
            {
                _logger.LogInformation($"User {currentUser} attempted to post a kweet as user {textKweet.UserId}.");
                return Unauthorized($"User {currentUser} attempted to post a kweet as user {textKweet.UserId}.");
            }

            try
            {
                await _kweetService.EditTextKweetAsync(textKweet);
            }
            catch (Exception e)
            {
                _logger.LogError("Something went wrong with the kweet service while editing a kweet", e.Message);
                return StatusCode(500);
            }

            return NoContent();
        }
    }
}