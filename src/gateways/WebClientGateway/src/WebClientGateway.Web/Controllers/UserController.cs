﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using WebClientGateway.Web.Config;
using WebClientGateway.Web.Grpc;
using WebClientGateway.Web.Grpc.KweetReading;
using WebClientGateway.Web.Models;

namespace WebClientGateway.Web.Controllers
{
    [Route("users")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IKweetReadingService _kweetReadingService;
        private readonly ILogger<UserController> _logger;

        public UserController(
            IUserService userService, 
            IKweetReadingService kweetReadingService,
            ILogger<UserController> logger)
        {
            _userService = userService;
            _kweetReadingService = kweetReadingService;
            _logger = logger;
        }

        [HttpGet]
        [Authorize(Roles = "moderator")]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(IEnumerable<AuthUser>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAll()
        {
            try
            {
                var users = await _userService.GetAllUsersAsync();
                return Ok(users);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return StatusCode(500);
            }
        }

        [HttpGet]
        [Route("kweets/{userId}")]
        [ProducesResponseType(typeof(IEnumerable<TextKweetViewModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetKweetsFromUser(string userId)
        {
            try
            {
                var userKweets = await _kweetReadingService.GetKweetsFromUser(userId);
                return Ok(userKweets);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return StatusCode(500);
            }
        }
    }
}
