﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebClientGateway.Web.Models;

namespace WebClientGateway.Web.Grpc.KweetReading
{
    public interface IKweetReadingService
    {
        Task<IEnumerable<TextKweetViewModel>> GetKweetsFromUser(string userId);
    }
}
