﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Grpc.Core;
using GrpcKweetReading;
using Microsoft.Extensions.Logging;
using WebClientGateway.Web.Models;

namespace WebClientGateway.Web.Grpc.KweetReading
{
    public class KweetReadingService : IKweetReadingService
    {
        private readonly KweetReadingGrpc.KweetReadingGrpcClient _client;
        private readonly ILogger<KweetReadingService> _logger;

        public KweetReadingService(
            KweetReadingGrpc.KweetReadingGrpcClient client,
            ILogger<KweetReadingService> logger)
        {
            _client = client;
            _logger = logger;
        }

        public async Task<IEnumerable<TextKweetViewModel>> GetKweetsFromUser(string userId)
        {
            _logger.LogInformation($"Starting gRPC call to other service for getting kweets from user for user {userId}");

            try
            {
                var request = MapToGetTextKweetsFromUserRequest(userId);
                var response = await _client.GetTextKweetsFromUserAsync(request);

                return MapToListOfTextKweets(response);
            }
            catch (RpcException e)
            {
                _logger.LogError(e.Message);
                throw;
            }
        }

        private static GetTextKweetsFromUserRequest MapToGetTextKweetsFromUserRequest(string userId)
        {
            return new GetTextKweetsFromUserRequest
            {
                UserId = userId
            };
        }

        private IEnumerable<TextKweetViewModel> MapToListOfTextKweets(GetTextKweetsFromUserResponse response)
        {
            return response.TextKweetsFromUser.Select(grpcTextKweet => new TextKweetViewModel
            {
                Id = grpcTextKweet.Id,
                Text = grpcTextKweet.Text,
                UserId = grpcTextKweet.UserId,
                Username = grpcTextKweet.Username
            });
        }
    }
}
