﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WebClientGateway.Web.Models;

namespace WebClientGateway.Web.Grpc
{
    public interface IUserService
    {
        Task<IEnumerable<AuthUser>> GetAllUsersAsync();
    }
}
