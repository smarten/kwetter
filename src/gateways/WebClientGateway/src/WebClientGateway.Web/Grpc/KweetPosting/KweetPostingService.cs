﻿using System.Threading.Tasks;
using Grpc.Core;
using GrpcKweetPosting;
using Microsoft.Extensions.Logging;
using WebClientGateway.Web.Models;

namespace WebClientGateway.Web.Grpc.KweetPosting
{
    public class KweetPostingService : IKweetPostingService
    {
        private readonly KweetPostingGrpc.KweetPostingGrpcClient _client;
        private readonly ILogger<KweetPostingService> _logger;

        public KweetPostingService(
            KweetPostingGrpc.KweetPostingGrpcClient client, 
            ILogger<KweetPostingService> logger)
        {
            _client = client;
            _logger = logger;
        }

        public async Task PostTextKweetAsync(TextKweetViewModel kweet)
        {
            _logger.LogInformation("Starting call to post kweet for user with id {userId}", kweet.UserId);

            try
            {
                await _client.PostTextKweetAsync(MapToPostTextKweetRequest(kweet));
            }
            catch (RpcException e)
            {
                _logger.LogError(e.Message);
                throw;
            }
        }

        public async Task EditTextKweetAsync(TextKweetViewModel kweet)
        {
            _logger.LogInformation("Starting call to edit kweet for user with id {userId}", kweet.UserId);

            try
            {
                await _client.EditTextKweetAsync(MapToEditTextKweetRequest(kweet));
            }
            catch (RpcException e)
            {
                _logger.LogError(e.Message);
                throw;
            }
        } 

        private static PostTextKweetRequest MapToPostTextKweetRequest(TextKweetViewModel kweet)
        {
            return new PostTextKweetRequest
            {
                Text = kweet.Text,
                UserId = kweet.UserId
            };
        }

        private static EditTextKweetRequest MapToEditTextKweetRequest(TextKweetViewModel kweet)
        {
            return new EditTextKweetRequest
            {
                Text = kweet.Text,
                TextKweetId = kweet.Id,
                UserId = kweet.UserId
            };
        }
    }
}
