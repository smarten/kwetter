﻿using System.Threading.Tasks;
using WebClientGateway.Web.Models;

namespace WebClientGateway.Web.Grpc.KweetPosting
{
    public interface IKweetPostingService
    {
        Task PostTextKweetAsync(TextKweetViewModel kweet);

        Task EditTextKweetAsync(TextKweetViewModel kweet);
    }
}
