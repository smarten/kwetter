﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WebClientGateway.Web.Config
{
    public class UrlsConfig
    {
        public string GrpcModerator { get; set; }
        public string KwetterWebClient { get; set; }
        public string GrpcKweetPosting { get; set; }
        public string GrpcKweetReading { get; set; }
    }
}
