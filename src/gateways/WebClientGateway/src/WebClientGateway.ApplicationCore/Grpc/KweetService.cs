﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Grpc.Core;
using GrpcKweet;
using Microsoft.Extensions.Logging;
using WebClientGateway.Web.Models;

namespace WebClientGateway.Web.Grpc
{
    public class KweetService : IKweetService
    {
        private readonly KweetGrpc.KweetGrpcClient _client;
        private readonly ILogger<KweetService> _logger;

        public KweetService(
            KweetGrpc.KweetGrpcClient client, 
            ILogger<KweetService> logger)
        {
            _client = client;
            _logger = logger;
        }

        public async Task PostTextKweetAsync(TextKweetViewModel kweet)
        {
            _logger.LogInformation("Starting call to post kweet for user with id {userId}", kweet.UserId);

            try
            {
                await _client.PostTextKweetAsync(MapToTextKweetRequest(kweet));
            }
            catch (RpcException e)
            {
                _logger.LogError(e.ToString());
                throw;
            }
        }

        private static PostTextKweetRequest MapToTextKweetRequest(TextKweetViewModel kweet)
        {
            return new PostTextKweetRequest
            {
                Text = kweet.Text,
                UserId = kweet.UserId
            };
        }
    }
}
