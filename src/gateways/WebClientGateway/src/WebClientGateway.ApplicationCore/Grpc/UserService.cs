﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Grpc.Core;
using Grpc.Net.Client;
using GrpcUser;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using WebClientGateway.Web.Config;
using WebClientGateway.Web.Models;

namespace WebClientGateway.Web.Grpc
{
    public class UserService : IUserService
    {
        private readonly UserGrpc.UserGrpcClient _client;
        private readonly ILogger<UserService> _logger;

        public UserService(
            UserGrpc.UserGrpcClient client,
            ILogger<UserService> logger)
        {
            _client = client;
            _logger = logger;
        }


        public async Task<IEnumerable<AuthUser>> GetAllUsersAsync()
        {
            _logger.LogInformation("Trying to get all users...");

            try
            {
                var response = await _client.GetAllUsersAsync(new GetAllUsersRequest());
                return MapToUsers(response);
            }
            catch (RpcException e)
            {
                _logger.LogError(e.ToString());
                throw;
            }
        }

        private static IEnumerable<AuthUser> MapToUsers(GetAllUsersResponse response)
        {
            return response.Users.Select(user => new AuthUser
            {
                Email = user.Email,
                Username = user.Username,
                Roles = user.Roles
            });
        }
    }
}
