﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using FluentAssertions;
using Google.Protobuf.Collections;
using Grpc.Core;
using Grpc.Core.Testing;
using Grpc.Net.Client;
using GrpcUser;
using Microsoft.Extensions.Logging.Abstractions;
using Moq;
using WebClientGateway.Web.Grpc;
using WebClientGateway.Web.Grpc.User;
using WebClientGateway.Web.Models;
using Xunit;

namespace WebClientGateway.Web.UnitTests.Grpc
{
    public class UserServiceTest
    {
        [Fact]
        public async void Should_get_users_through_grpc_server_and_map_to_correct_auth_user_list()
        {
            // Arrange
            var expectedUsers = new List<AuthUser>
            {
                new AuthUser
                {
                    Email = "johndoe@gmail.com",
                    Username = "johndoe",
                    Roles = new[] {"moderator"}
                },
                new AuthUser
                {
                    Email = "janedoe@gmail.com",
                    Username = "jane-doe",
                    Roles = new[] {"user"}
                }
            };

            var actualUsers = new RepeatedField<UserResponse>
            {
                new UserResponse
                {
                    Email = "johndoe@gmail.com",
                    Username = "johndoe",
                    Roles = {"moderator"}
                },
                new UserResponse
                {
                    Email = "janedoe@gmail.com",
                    Username = "jane-doe",
                    Roles = {"user"}
                }
            };
            var getAllUsersResponse = new GetAllUsersResponse();
            getAllUsersResponse.Users.AddRange(actualUsers);

            var fakeGetAllUsersCall = TestCalls.AsyncUnaryCall(
                Task.FromResult(getAllUsersResponse), 
                Task.FromResult(new Metadata()),
                () => Status.DefaultSuccess, 
                () => new Metadata(), 
                () => { });

            var userGrpcClientMock = new Mock<UserGrpc.UserGrpcClient>();
            userGrpcClientMock
                .Setup(client => client.GetAllUsersAsync(
                    It.IsAny<GetAllUsersRequest>(),
                    It.IsAny<Metadata>(),
                    It.IsAny<DateTime?>(),
                    It.IsAny<CancellationToken>()
                ))
                .Returns(fakeGetAllUsersCall);

            var sut = new UserService(userGrpcClientMock.Object, new NullLogger<UserService>());

            // Act
            var userResponse = await sut.GetAllUsersAsync();

            // Assert
            userResponse.Should().BeEquivalentTo(expectedUsers, config => config.WithStrictOrdering());
        }
    }
}
