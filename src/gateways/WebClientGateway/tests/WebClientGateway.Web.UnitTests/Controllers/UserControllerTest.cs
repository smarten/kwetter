﻿using System;
using System.Collections.Generic;
using FluentAssertions;
using Grpc.Core;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging.Abstractions;
using Moq;
using WebClientGateway.Web.Controllers;
using WebClientGateway.Web.Grpc;
using WebClientGateway.Web.Grpc.KweetReading;
using WebClientGateway.Web.Models;
using Xunit;

namespace WebClientGateway.Web.UnitTests.Controllers
{
    public class UserControllerTest
    {
        [Fact]
        public async void Should_get_all_users_and_return_200_status_code()
        {
            // Arrange
            var users = new List<AuthUser>()
            {
                new AuthUser
                {
                    Email = "johndoe@gmail.com",
                    Username = "john_doe",
                    Roles = new List<string>
                    {
                        "User"
                    }
                },
                new AuthUser
                {
                    Email = "janedoe@hotmail.com",
                    Username = "jane_doe",
                    Roles = new List<string>
                    {
                        "Moderator"
                    }
                }
            };

            var userGrpcService = new Mock<IUserService>();
            userGrpcService.Setup(service => service.GetAllUsersAsync()).ReturnsAsync(users);

            var kweetReadingGrpcServiceMock = new Mock<IKweetReadingService>();

            var sut = new UserController(userGrpcService.Object, kweetReadingGrpcServiceMock.Object, new NullLogger<UserController>());

            // Act
            var userResponse = await sut.GetAll();

            // Assert
            userResponse
                .Should().BeOfType<OkObjectResult>().Which.Value
                .Should().BeOfType<List<AuthUser>>().Which
                .Should().BeEquivalentTo(users, config => config.WithStrictOrdering());
        }

        [Fact]
        public async void Should_give_500_status_code_with_unknown_error_in_getting_all_users()
        {
            // Arrange
            var userGrpcServiceMock = new Mock<IUserService>();
            var kweetReadingGrpcServiceMock = new Mock<IKweetReadingService>();

            userGrpcServiceMock.Setup(service => service.GetAllUsersAsync()).Throws<Exception>();
            
            var sut = new UserController(userGrpcServiceMock.Object, kweetReadingGrpcServiceMock.Object, new NullLogger<UserController>());

            // Act
            var response = await sut.GetAll();

            // Assert
            response
                .Should().BeOfType<StatusCodeResult>().Which.StatusCode
                .Should().Be(500);
        }
    }
}
