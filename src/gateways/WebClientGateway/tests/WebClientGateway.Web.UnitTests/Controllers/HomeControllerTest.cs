﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using WebClientGateway.Web.Controllers;
using Xunit;

namespace WebClientGateway.Web.UnitTests.Controllers
{
    public class HomeControllerTest
    {
        [Fact]
        public void Root_domain_should_reroute_to_swagger_docs()
        {
            var sut = new HomeController();

            var response = sut.Index();

            response
                .Should().BeOfType<RedirectResult>().Which.Url
                .Should().Be("~/swagger");
        }
    }
}
